#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "genrnd.h"
#include "pomiar_czasu.h"
#include <unistd.h>

void randivectasc1(int count, int min, int max, int *vect);
void randivectasc2(int count, int min, int max, int *vect);
int checkifasc(int *vect, int count);

// <min, max)
void randivectasc1(int count, int min, int max, int *vect)
{
  int range = max - min;
  int in, im;

  im = 0;

  for (in = 0; in < range && im < count; ++in)
  {
    int rn = range - in;
    int rm = count - im;
    if (rand() % rn < rm)
      vect[im++] = in + min; /* +min since your range begins from min */
  }
}

void randivectasc2(int count, int min, int max, int *vect)
{
  randivectasc(count, min, max, vect);
}

int checkifasc(int *vect, int count)
{
  int i;
  for (i = 0; i < count-1; ++i)
  {
    if (vect[i] >= vect[i+1])
    {
      return 0;
    }
  }

  return 1;
}

int main(int argc, char *argv[])
{
  if (argc != 4)
  {
    printf("Count, min, max (only) required!");
    exit(0);
  }

  int count = atoi(argv[1]);
  int min = atoi(argv[2]);
  int max = atoi(argv[3]);

  printf("Count: %d\n", count);
  printf("Min: %d\n", min);
  printf("Max: %d\n", max);

  inicjuj_czas();

  int *vect = calloc(count, sizeof(int));

  double t1 = czas_zegara();
  randivectasc1(count, min, max, vect);
  double t2 = czas_zegara();
  int asc = checkifasc(vect, count);
  printf ("Old version time: %lf, asc %d\n", t2 - t1, asc);

  memset(vect, 0, sizeof(*vect)*count);

  double t3 = czas_zegara();
  randivectasc2(count, min, max, vect);
  double t4 = czas_zegara();
  asc = checkifasc(vect, count);
  printf ("New version time: %lf, asc %d\n", t4 - t3, asc);

  return 1;
}