#include "genrnd.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define ITER 1000000

void test_randint1()
{
  printf("Test of rand int generator - only zeros\n");

  int i, max, min, res;

  max = 0;
  min = 0;
  for (i = 0; i < ITER; ++i)
  {
    res = randint(min, max);
    assert(res == 0);
  }

  printf("Passed\n");
}

void test_randint2()
{
  int i, max, min, res;

  max = 1000;
  min = 0;

  printf("Test of rand int generator - x >= min (%d) to x < max (%d)\n", min, max);
  for (i = 0; i < ITER; ++i)
  {
    res = randint(min, max);
    assert(res < max);
    assert(res >= min);
  }

  printf("Passed\n");
}

void test_randint3()
{
  int i, max, min, res;

  max = -1;
  min = 0;

  printf("Test of rand int generator - max (%d) < min (%d) -> all zeros\n", max, min);
  for (i = 0; i < ITER; ++i)
  {
    res = randint(min, max);
    assert(res == 0);
  }

  printf("Passed\n");
}

void test_randivectasc()
{
  int min, max, i, n, j, prev;

  min = 0;
  max = 50;
  n = 20;

  printf("Test of random vector asc - each x >= min (%d) to x < max (%d)\n", min, max);
  printf("Test of random vector asc - elements strictly ascending\n");

  for (i = 0; i < ITER; ++i)
  {
    int *pos = malloc(n*sizeof(int));
    randivectasc(n, min, max, pos);
    prev = min - 1;
    for (j = 0; j < n; ++j) 
    {
      assert(prev < pos[j]);
      assert(pos[j] >= min);
      assert(pos[j] < max);
      prev = pos[j];
    }
  }
  printf("Passed\n");
}

int main(void)
{
  srand(time(NULL));
  test_randint1();
  test_randint2();
  test_randint3();
  test_randivectasc();
  return 0;
}