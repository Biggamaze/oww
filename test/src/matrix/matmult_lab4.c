#include "matmult.h"
#include "genmat.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "pomiar_czasu.h"

#define N 1000000
#define NNZW 100

double *ones(int n)
{
  double *arr = malloc(n * sizeof(double));

  int i;
  for (i = 0; i < n; ++i)
  {
    arr[i] = 1.0;
  }

  return arr;
}

int main(void)
{
  inicjuj_czas();

  printf("Inicjowanie macierzy ...\n");
  double t1z = czas_zegara();
  double t1c = czas_CPU();

  CSrc *cs = sparsemat_crs(N, NNZW);
  // double *full = tofullmat(cs);
  double *ons = ones(N);
  double *res = calloc(N, sizeof(double));

  double t2z = czas_zegara();
  double t2c = czas_CPU();
  printf("Zainicjowano w %lf (CPU: %lf)\n", t2z - t1z, t2c - t1c);

  // GESTA

  // printf("Mnożenie macierz wektor macierzy gęstej\n");
  // t1z = czas_zegara();
  // t1c = czas_CPU();

  // fullmatvectmult(full, ons, N, res);

  // t2z = czas_zegara();
  // t2c = czas_CPU();
  // printf("Pomnożono w %lf (CPU: %lf) ... %lf\n", t2z - t1z, t2c - t1c, res[0] - res[1]);

  // RZADKA

  printf("Mnożenie macierz wektor macierzy rzadkiej\n");
  t1z = czas_zegara();
  t1c = czas_CPU();

  csmatvectmul(cs, ons, res);

  t2z = czas_zegara();
  t2c = czas_CPU();
  printf("Pomnożono w %lf (CPU: %lf) ... %lf\n", t2z - t1z, t2c - t1c, res[0] - res[1]);

  return 1;
}
