#include "matmult.h"
#include "genmat.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "pomiar_czasu.h"

#define N 10000
#define ITER 25
#define NNZW 25

double *ones(int n)
{
  double *arr = malloc(n * sizeof(double));

  int i;
  for (i = 0; i < n; ++i)
  {
    arr[i] = 1.0;
  }

  return arr;
}

void assertmatequal(double *one, double *two, int size)
{
  int i;
  for (i = 0; i < size; ++i)
  {
    assert(one[i] == two[i]);
  }
}

void test_matmult1()
{
  int i;

  inicjuj_czas();
  printf("Test case 1: \n Matrix Vector multiply comparison \n");
  for (i = 0; i < ITER; ++i)
  {
    CSrc *cs = sparsemat_crs(N, NNZW);
    double *full = tofullmat(cs);

    double *resf = calloc(N, sizeof(double));
    double *ress = calloc(N, sizeof(double));

    double *ons = ones(N);

    fullmatvectmult(full, ons, N, resf);
    csmatvectmul(cs, ons, ress);

    assertmatequal(resf, ress, N);

    free(cs->ind);
    free(cs->ptr);
    free(cs->SData);
    free(cs->val);
    free(cs);
    free(full);
    free(resf);
    free(ress);
  }
  printf("Passed\n");
  drukuj_czas();
}

void test_matmult2()
{
  int i;

  inicjuj_czas();

  double czas_C1 = czas_C();
  double czas_zegara1 = czas_zegara();
  double czas_CPU1 = czas_CPU();
	
  printf("Test case 2: \n Matrix Vector multiply comparison \n");
  for (i = 0; i < ITER; ++i)
  {
    CSrc *cs = sparsemat_crs(N, NNZW);
    double *full = tofullmat(cs);

    double *resf = calloc(N, sizeof(double));
    double *ress = calloc(N, sizeof(double));
    double *ons = ones(N);

    fullmatvectmult(full, ons, N, resf);
    csmatvectmul(cs, ons, ress);

    assertmatequal(resf, ress, N);

    free(cs->ind);
    free(cs->ptr);
    free(cs->SData);
    free(cs->val);
    free(cs);
    free(full);
    free(resf);
    free(ress);
  }
  printf("Passed\n");

  double czas_C2 = czas_C();
  double czas_zegara2 = czas_zegara();
  double czas_CPU2 = czas_CPU();

  printf("czas standardowy = %lf\n", czas_C2 - czas_C1);
  printf("czas CPU         = %lf\n", czas_CPU2 - czas_CPU1);
  printf("czas zegarowy    = %lf\n", czas_zegara2 - czas_zegara1);
}

void test_matmult3()
{
  int i;

  inicjuj_czas();

  printf("Test case 2: \n Matrix Vector multiply comparison \n");
  for (i = 0; i < ITER; ++i)
  {
    CSrc *cs = sparsemat_crs(N, NNZW);
    double *full = tofullmat(cs);

    double *resf = calloc(N, sizeof(double));
    double *ress = calloc(N, sizeof(double));
    double *ons = ones(N);

    fullmatvectmult(full, ons, N, resf);
    csmatvectmul(cs, ons, ress);

    assertmatequal(resf, ress, N);

    free(cs->ind);
    free(cs->ptr);
    free(cs->SData);
    free(cs->val);
    free(cs);
    free(full);
    free(resf);
    free(ress);
  }
  printf("Passed\n");
  drukuj_czas();
}

int main(void)
{
  test_matmult1();
  test_matmult2();
  test_matmult3();
}
