#include "matmult.h"
#include "genmat.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "pomiar_czasu.h"

#define N 100000
#define NNZW 5000
#define D 50

double *ones(int n)
{
  double *arr = malloc(n * sizeof(double));

  int i;
  for (i = 0; i < n; ++i)
  {
    arr[i] = 1.0;
  }

  return arr;
}

double reduce(double *vect){
  int i;
  double res = 0;
  for (i = 0; i < N; ++i){
    res += vect[i];
  }
  return res;
}

int main(void)
{
  inicjuj_czas();

  double res;

  printf("Inicjowanie macierzy ...\n");
  double t1z = czas_zegara();
  double t1c = czas_CPU();

  CSrc *cs = sparsemat_crs(N, NNZW);
  CSrc *csp = sparseband_crs(N, D, NNZW);
  double *full = tofullmat(cs);
  double *fullp = tofullmat(csp);
  double *ons = ones(N);
  double *resv = calloc(N, sizeof(double));

  double t2z = czas_zegara();
  double t2c = czas_CPU();
  printf("Zainicjowano w %lf (CPU: %lf)\n", t2z - t1z, t2c - t1c);

  printf("Mnożenie macierz wektor macierzy gęstej\n");
  t1z = czas_zegara();
  t1c = czas_CPU();

  fullmatvectmult(full, ons, N, resv);

  t2z = czas_zegara();
  t2c = czas_CPU();
  res = reduce(resv);
  printf("Pomnożono w %lf (CPU: %lf) ... Wynik: %lf\n", t2z - t1z, t2c - t1c, res);

  printf("Mnożenie macierz wektor macierzy gęstej pasmowej\n");
  t1z = czas_zegara();
  t1c = czas_CPU();

  fullmatvectmult(fullp, ons, N, resv);

  t2z = czas_zegara();
  t2c = czas_CPU();
  res = reduce(resv);
  printf("Pomnożono w %lf (CPU: %lf) ... Wynik: %lf\n", t2z - t1z, t2c - t1c, res);

  printf("Mnożenie macierz wektor macierzy rzadkiej\n");
  t1z = czas_zegara();
  t1c = czas_CPU();

  csmatvectmul(cs, ons, resv);

  t2z = czas_zegara();
  t2c = czas_CPU();
  res = reduce(resv);
  printf("Pomnożono w %lf (CPU: %lf) ... Wynik: %lf\n", t2z - t1z, t2c - t1c, res);

  printf("Mnożenie macierz wektor macierzy rzadkiej pasmowej\n");
  t1z = czas_zegara();
  t1c = czas_CPU();

  csmatvectmul(csp, ons,resv);

  t2z = czas_zegara();
  t2c = czas_CPU();
  res = reduce(resv);
  printf("Pomnożono w %lf (CPU: %lf) ... Wynik: %lf\n", t2z - t1z, t2c - t1c, res);

  return 1;
}