#include "matmult.h"
#include "genmat.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "pomiar_czasu.h"
#include <string.h>

#define N 5000
#define NNZW 5000
#define D 50
#define IPC 16
#define CORES 2
#define CORE_HZ 2.9 * 1000 * 1000 * 1000

double *ones(int n)
{
    int i;
    
    double *arr = malloc(n * sizeof(double));
    for (i = 0; i < n; ++i)
    {
        arr[i] = 1.0;
    }
    return arr;
}

void assertmatequal(double *one, double *two, int size)
{
    int i;
    for (i = 0; i < size; ++i)
    {
        assert(one[i] == two[i]);
    }
}

void zero_vector(double *vect, int length)
{
    memset(vect, 0.0, length * sizeof(double));
}

int main(void)
{
    inicjuj_czas();

    printf("Inicjowanie macierzy ...\n");
    double t1z = czas_zegara();

    CSrc *cs = sparsemat_crs(N, NNZW);
    double *full = tofullmat(cs);
    double *ons = ones(N);
    double *resv = calloc(N, sizeof(double));
    double *resv1 = calloc(N, sizeof(double));
    double *resv2 = calloc(N, sizeof(double));
    double *resv3 = calloc(N, sizeof(double));

    double t2z = czas_zegara();
    printf("Zainicjowano w %lf \n", t2z - t1z);
    int i, iter = 200;
    double texe = 0;
    unsigned int mem_access_noopt = 4*N*NNZW; // O0 = 13n*nnzw, O3 = 4n*nnzw 
    unsigned int mem_access_opt = 3*N*NNZW; // O0 = 10n*nnzw , O3 = 3n*nnzw

    printf("Mnożenie macierz wektor macierzy rzadkiej - brak optymalizacji ręcznej\n");

    for (i = 0; i < iter; ++i)
    {
        zero_vector(resv, N);
        t1z = czas_zegara();
        csmatvectmul(cs, ons, resv);
        t2z = czas_zegara();

        texe += t2z - t1z;
    }

    texe /= iter;
    double flop_real = (2 * NNZW) / texe;
    printf("\nCzas wykonania =    %lf s\n", texe);
    printf("Czas dostępu =      %lf ns\n", texe / mem_access_noopt * 1e9);
    printf("Wydajność =         %lf MFLOPS\n", flop_real / 1024 / 1024);
    printf("%% teoretycznej =    %lf %%\n\n", flop_real / (CORE_HZ * IPC * CORES) * 100);

    texe = 0;
    printf("Mnożenie macierz wektor macierzy rzadkiej - optymalizacje ręczne\n");

    for (i = 0; i < iter; ++i)
    {
        zero_vector(resv2, N);
        t1z = czas_zegara();
        csmatvectmul_opt(cs, ons, resv2);
        t2z = czas_zegara();

        texe += t2z - t1z;
    }

    texe /= iter;
    flop_real = (2 * NNZW) / texe;
    printf("\nCzas wykonania =    %lf s\n", texe);
    printf("Czas dostępu =      %lf ns\n", texe / mem_access_opt * 1e9);
    printf("Wydajność =         %lf MFLOPS\n", flop_real / 1024 / 1024);
    printf("%% teoretycznej =    %lf %%\n\n", flop_real / (CORE_HZ * IPC * CORES) * 100);

    printf("Mnożenie macierz wektor macierzy rzadkiej - OpenMP\n");

    for (i = 0; i < iter; ++i)
    {
        zero_vector(resv3, N);
        t1z = czas_zegara();
        csmatvectmul_par(cs, ons, resv3);
        t2z = czas_zegara();

        texe += t2z - t1z;
    }

    texe /= iter;
    flop_real = (2 * NNZW) / texe;
    printf("\nCzas wykonania =    %lf s\n", texe);
    printf("Czas dostępu =      %lf ns\n", texe / mem_access_opt * 1e9);
    printf("Wydajność =         %lf MFLOPS\n", flop_real / 1024 / 1024);
    printf("%% teoretycznej =    %lf %%\n\n", flop_real / (CORE_HZ * IPC * CORES) * 100);

    printf("Mnożenie macierz wektor macierzy gęstej\n");
    t1z = czas_zegara();
    fullmatvectmult(full, ons, N, resv1);
    t2z = czas_zegara();
    printf("Pomnożono w %lf \n", t2z - t1z);

    printf("\t\t---TEST---\n");
    assertmatequal(resv, resv1, N);
    assertmatequal(resv, resv2, N);
    assertmatequal(resv1, resv2, N);
    assertmatequal(resv1, resv3, N);

    printf("Przeszedł!\n");

    return 1;
}