#include "genmat.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void testfull1() {
  int n = 6;
  int nnzw = 3;
  
  CSrc *cs = sparsemat_crs(n, nnzw);
  double* arr = tofullmat(cs);
  printfull(arr, n);
}

int main() {
  srand(time(NULL));
  testfull1();

  return 0;
}