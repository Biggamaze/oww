#include "genmat.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
  srand(time(NULL));
  int i;
  int n = 7;
  int nnzw = 3;
  int d = 2;

  printf("N = %d\nNNZW= %d\nD=%d\n\n", n, nnzw, d);

  CSrc *crs = sparseband_crs(n, d, nnzw);
  printcrsfull(crs);

  printf("\n\nval:");
  for (i = 0; i < n*nnzw; ++i) {
    printf (" %lf ", crs -> val[i]);
  }
  printf("\ncol_ind: ");
  for (i = 0; i < n*nnzw; ++i) {
    printf(" %d ", crs -> ind[i]);
  }
  printf("\nrow_ptr: ");
  for (i = 0; i < n+1; ++i) {
    printf(" %d ", crs -> ptr[i]);
  }

  printf("\n");
  free(crs -> ptr);
  free((*crs).ind);
  free(crs -> val);
  free(crs);

  return 0;
}

int dump(void) {
  srand(time(NULL));
  int n = 5;
  int nnzw = 2;
  int i;

  CSrc *crs = sparsemat_crs(n, nnzw);
  printf("val:");
  for (i = 0; i < n*nnzw; ++i) {
    printf (" %lf ", crs -> val[i]);
  }
  printf("\ncol_ind: ");
  for (i = 0; i < n*nnzw; ++i) {
    printf(" %d ", crs -> ind[i]);
  }
  printf("\nrow_ptr: ");
  for (i = 0; i < n+1; ++i) {
    printf(" %d ", crs -> ptr[i]);
  }

  printf("\n");
  free(crs -> ptr);
  free((*crs).ind);
  free(crs -> val);
  free(crs);

  return 1;
}