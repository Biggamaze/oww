#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define WYMIAR 5000
#define ROZMIAR 25000000

void mat_vec(double* a, double* x, double* y, int n);

main ()
{
  static double a[ROZMIAR],x[WYMIAR],y[WYMIAR],z[WYMIAR];
  int n,i,j;
  const int ione=1;
  const double done=1.0;
  const double dzero=0.0;


  for(i=0;i<ROZMIAR;i++) a[i]=1.0*i;
  for(i=0;i<WYMIAR;i++) x[i]=1.0*(WYMIAR-i);

  n=WYMIAR;

  printf("poczatek\n");

  mat_vec(a,x,y,n);

  printf("koniec\n");

  // test
  printf("TEST\n");

  for(i=0;i<n;i++){
    z[i]=0.0;
    for(j=0;j<n;j++){
      z[i]+=a[i+n*j]*x[j];
    }
  }

  // test - jesli dostepna biblioteka LAPACK Intela
  //printf("MKL\n");

  //dgemv_("N", &n, &n, &done, a, &n, x, &ione, &dzero, z, &ione);

  for(i=0;i<WYMIAR;i++){
    if(fabs(y[i]-z[i])>1.e-9*z[i]) printf("Blad!\n");
  }

}
