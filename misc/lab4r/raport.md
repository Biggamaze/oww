## 1. Tabele z wynikami

### Macierz gęsta N = 10 000, NNZW = 10 000

| Dana | Wartość |
|---|:---:|
| Czas wykonania |0.3 s|
| Wydajność GFLOPS |1.31 GFlop/s|
| Liczba dost. do pam. |5*1e9|
| Średni czas poj. dostępu |3e-10 s|
| Liczba trafień/chybień L1 |5075159613/25010997|
| Liczba trafień/chybień LL |12509549/12501473|

### Macierz rzadka N = 1 000 000, NNZW = 100

| Dana | Wartość |
|---|:---:|
| Czas wykonania |3.31 s|
| Wydajność GFLOPS |0.15 GFlop/s|
| Liczba dost. do pam. |4*1e9|
| Średni czas poj. dostępu |6.8e-10|
| Liczba trafień/chybień L1 |6583668326/56430038|
| Liczba trafień/chybień LL |12863399/43566654|

## 2. Kod mnożenia macierz * wektor
```c
#include "genmat.h"
#include <time.h>
#include <stdlib.h>

void fullmatvectmult(double *fullmat, double *vect, int size, double *res) {
  int i,j;

  for (i = 0; i < size; ++i) {
    for (j = 0; j < size; ++j) {
      res[i] += fullmat[i*size + j] * vect[j];
    }
  }
}

void csmatvectmul(CSrc *cs, double *vect, double *res) {
  int i,j;

  int *ptr = cs->ptr;
  int *ind = cs->ind;
  double *val = cs->val;

  for (i = 0; i < cs->SData->n; ++i) {
    for (j = ptr[i]; j < ptr[i+1]; ++j) {
      res[i] += vect[ind[j]] * val[j];
    }
  }
}
```