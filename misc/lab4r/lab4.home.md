### 1. Gęste
----------

N = 10000
NNZW = 10000


#### Sama generacja

*gprof*
```bash
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
 44.49      0.92     0.92                             randivectasc_full
 16.93      1.27     0.35                             randdvect
 16.44      1.61     0.34                             tofullmat
 14.03      1.90     0.29                             randdbl
  6.77      2.04     0.14                             randbase
  1.45      2.07     0.03                             randint
  0.00      2.07     0.00    10000     0.00     0.00  finddiag
  0.00      2.07     0.00    10000     0.00     0.00  makerow
  0.00      2.07     0.00        1     0.00     0.00  alloccs
```
*perf stat*
```bash
 Performance counter stats for 'bin/matmult_lab4.test':

       3077,297323      task-clock (msec)         #    0,997 CPUs utilized          
               162      context-switches          #    0,053 K/sec                  
                 3      cpu-migrations            #    0,001 K/sec                  
           488 366      page-faults               #    0,159 M/sec                  
    10 456 088 424      cycles                    #    3,398 GHz                    
    20 345 736 301      instructions              #    1,95  insn per cycle         
     4 635 228 344      branches                  # 1506,266 M/sec                  
         8 380 120      branch-misses             #    0,18% of all branches        

       3,086272359 seconds time elapsed
```
*valgrind*
```bash
==13117== Memcheck, a memory error detector
==13117== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==13117== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==13117== Command: bin/matmult_lab4.test
==13117== 
Inicjowanie macierzy ...
==13117== Warning: set address range perms: large range [0x562d040, 0x3511d840) (defined)
==13117== Warning: set address range perms: large range [0x3511e040, 0x4ce96440) (defined)
==13117== Warning: set address range perms: large range [0x59e43040, 0x89933840) (defined)
Zainicjowano w 77.681061 (CPU: 77.207578)
==13117== 
==13117== HEAP SUMMARY:
==13117==     in use at exit: 2,000,120,044 bytes in 7 blocks
==13117==   total heap usage: 9 allocs, 2 frees, 2,000,127,962 bytes allocated
==13117== 
==13117== LEAK SUMMARY:
==13117==    definitely lost: 80,032 bytes in 2 blocks
==13117==    indirectly lost: 400,040,012 bytes in 3 blocks
==13117==      possibly lost: 1,600,000,000 bytes in 2 blocks
==13117==    still reachable: 0 bytes in 0 blocks
==13117==         suppressed: 0 bytes in 0 blocks
==13117== Rerun with --leak-check=full to see details of leaked memory
==13117== 
==13117== For counts of detected and suppressed errors, rerun with: -v
==13117== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

*valgrind --leak-check=full*
```bash
==13217== Memcheck, a memory error detector
==13217== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==13217== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==13217== Command: bin/matmult_lab4.test
==13217== 
Inicjowanie macierzy ...
==13217== Warning: set address range perms: large range [0x562d040, 0x3511d840) (defined)
==13217== Warning: set address range perms: large range [0x3511e040, 0x4ce96440) (defined)
==13217== Warning: set address range perms: large range [0x59e43040, 0x89933840) (defined)
Zainicjowano w 80.972772 (CPU: 80.275274)
==13217== 
==13217== HEAP SUMMARY:
==13217==     in use at exit: 2,000,120,044 bytes in 7 blocks
==13217==   total heap usage: 9 allocs, 2 frees, 2,000,127,962 bytes allocated
==13217== 
==13217== 80,000 bytes in 1 blocks are definitely lost in loss record 3 of 7
==13217==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==13217==    by 0x108A9B: ones (matmult_lab4.c:13)
==13217==    by 0x108B51: main (matmult_lab4.c:34)
==13217== 
==13217== 400,040,044 (32 direct, 400,040,012 indirect) bytes in 1 blocks are definitely lost in loss record 5 of 7
==13217==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==13217==    by 0x108E84: alloccs (sparsemat.c:71)
==13217==    by 0x108BCE: sparsemat_crs (sparsemat.c:17)
==13217==    by 0x108B33: main (matmult_lab4.c:32)
==13217== 
==13217== 800,000,000 bytes in 1 blocks are possibly lost in loss record 6 of 7
==13217==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==13217==    by 0x108E9E: alloccs (sparsemat.c:72)
==13217==    by 0x108BCE: sparsemat_crs (sparsemat.c:17)
==13217==    by 0x108B33: main (matmult_lab4.c:32)
==13217== 
==13217== 800,000,000 bytes in 1 blocks are possibly lost in loss record 7 of 7
==13217==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==13217==    by 0x10902E: tofullmat (fullmat.c:9)
==13217==    by 0x108B43: main (matmult_lab4.c:33)
==13217== 
==13217== LEAK SUMMARY:
==13217==    definitely lost: 80,032 bytes in 2 blocks
==13217==    indirectly lost: 400,040,012 bytes in 3 blocks
==13217==      possibly lost: 1,600,000,000 bytes in 2 blocks
==13217==    still reachable: 0 bytes in 0 blocks
==13217==         suppressed: 0 bytes in 0 blocks
==13217== 
==13217== For counts of detected and suppressed errors, rerun with: -v
==13217== ERROR SUMMARY: 4 errors from 4 contexts (suppressed: 0 from 0)
```
*valgrind --tool=cachegrind*
```bash
==14019== Cachegrind, a cache and branch-prediction profiler
==14019== Copyright (C) 2002-2017, and GNU GPL'd, by Nicholas Nethercote et al.
==14019== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==14019== Command: bin/matmult_lab4.test
==14019== 
--14019-- warning: L3 cache found, using its data for the LL simulation.
Inicjowanie macierzy ...
Zainicjowano w 116.121584 (CPU: 115.578680)
==14019== 
==14019== I   refs:      21,902,323,078
==14019== I1  misses:             1,358
==14019== LLi misses:             1,345
==14019== I1  miss rate:           0.00%
==14019== LLi miss rate:           0.00%
==14019== 
==14019== D   refs:      12,209,366,734  (9,106,444,679 rd   + 3,102,922,055 wr)
==14019== D1  misses:        50,445,364  (   19,052,816 rd   +    31,392,548 wr)
==14019== LLd misses:        50,006,593  (   18,753,364 rd   +    31,253,229 wr)
==14019== D1  miss rate:            0.4% (          0.2%     +           1.0%  )
==14019== LLd miss rate:            0.4% (          0.2%     +           1.0%  )
==14019== 
==14019== LL refs:           50,446,722  (   19,054,174 rd   +    31,392,548 wr)
==14019== LL misses:         50,007,938  (   18,754,709 rd   +    31,253,229 wr)
==14019== LL miss rate:             0.1% (          0.1%     +           1.0%  )
==14019== 
==14019== Branches:       2,794,878,746  (2,594,734,664 cond +   200,144,082 ind)
==14019== Mispredicts:       13,003,236  (   13,003,049 cond +           187 ind)
==14019== Mispred rate:             0.5% (          0.5%     +           0.0%   )

```
```bash
--------------------------------------------------------------------------------
I1 cache:         32768 B, 64 B, 8-way associative
D1 cache:         32768 B, 64 B, 8-way associative
LL cache:         3145728 B, 64 B, 12-way associative
Command:          bin/matmult_lab4.test
Data file:        misc/lab4r/msi/cachegrind.gesta.samagen
Events recorded:  Ir I1mr ILmr Dr D1mr DLmr Dw D1mw DLmw Bc Bcm Bi Bim
Events shown:     Ir I1mr ILmr Dr D1mr DLmr Dw D1mw DLmw Bc Bcm Bi Bim
Event sort order: Ir I1mr ILmr Dr D1mr DLmr Dw D1mw DLmw Bc Bcm Bi Bim
Thresholds:       0.1 100 100 100 100 100 100 100 100 100 100 100 100
Include dirs:     
User annotated:   
Auto-annotation:  off

--------------------------------------------------------------------------------
            Ir  I1mr  ILmr            Dr       D1mr       DLmr            Dw       D1mw       DLmw            Bc        Bcm          Bi Bim 
--------------------------------------------------------------------------------
21,902,323,078 1,358 1,345 9,106,444,679 19,052,816 18,753,364 3,102,922,055 31,392,548 31,253,229 2,594,734,664 13,003,049 200,144,082 187  PROGRAM TOTALS

--------------------------------------------------------------------------------
           Ir I1mr ILmr            Dr       D1mr       DLmr          Dw       D1mw       DLmw            Bc        Bcm          Bi Bim  file:function
--------------------------------------------------------------------------------
5,387,366,130    2    2 1,600,080,000          3          2 800,040,000          0          0   993,598,065 12,903,891           0   0  /build/glibc-OTsEL5/glibc-2.27/stdlib/random_r.c:random_r
4,400,220,000    4    4 1,800,090,000          2          1 400,020,000          0          0 1,000,050,000        114           0   0  /build/glibc-OTsEL5/glibc-2.27/stdlib/random.c:random
4,100,280,024    5    5 2,100,140,011 18,750,627 18,750,627 200,020,007 12,500,000 12,500,000   200,030,001     10,023           1   1  /home/radoslaw/Projects/oww/src/matrix/fullmat.c:tofullmat
3,000,190,000    2    2 1,500,060,000          0          0 500,080,000  6,259,997  6,250,000   300,010,000     10,648           0   0  ???:randivectasc_full
1,700,180,000    2    2   800,050,000          0          0 300,070,000 12,510,000 12,500,000   100,010,000     10,029           0   0  ???:randdvect
1,500,150,000    1    1   600,060,000          0          0 500,050,000          0          0             0          0           0   0  ???:randdbl
  800,080,000    1    1   300,030,000          0          0 200,020,000          0          0             0          0           0   0  ???:randbase
  800,040,000    1    1   200,010,000          0          0 200,010,000          0          0             0          0           0   0  /build/glibc-OTsEL5/glibc-2.27/stdlib/rand.c:rand
  200,031,631   35   31   200,010,123         18         10          24          1          1            10          5 200,010,094  31  ???:???
```
#### Razem z mnożeniem

**grprof**
<pre>
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 34.28      0.87     0.87                             randivectasc_full
 23.64      1.47     0.60        1   600.51   600.51  tofullmat
 <b>14.97      1.85     0.38        1   380.32   380.32  fullmatvectmult</b>
 13.40      2.19     0.34                             randdbl
 10.24      2.45     0.26                             randdvect
  3.55      2.54     0.09                             randbase
  0.00      2.54     0.00    10000     0.00     0.00  finddiag
  0.00      2.54     0.00    10000     0.00     0.00  makerow
  0.00      2.54     0.00        4     0.00     0.00  czas_CPU
  0.00      2.54     0.00        4     0.00     0.00  czas_zegara
  0.00      2.54     0.00        1     0.00     0.00  alloccs
  0.00      2.54     0.00        1     0.00     0.00  inicjuj_czas
  0.00      2.54     0.00        1     0.00     0.00  ones
  0.00      2.54     0.00        1     0.00     0.00  sparsemat_crs

</pre>

**perf stat**

```bash
Inicjowanie macierzy ...
Zainicjowano w 3.224143 (CPU: 2.839792)
Mnożenie macierz wektor macierzy gęstej
Pomnożono w 0.315380 (CPU: 0.315396) ... 0.000000

 Performance counter stats for 'bin/matmult_lab4.test':

       3601,228290      task-clock (msec)         #    1,000 CPUs utilized          
                17      context-switches          #    0,005 K/sec                  
                 1      cpu-migrations            #    0,000 K/sec                  
           488 402      page-faults               #    0,136 M/sec                  
    12 505 141 285      cycles                    #    3,472 GHz                    
    26 764 535 267      instructions              #    2,14  insn per cycle         
     4 839 480 606      branches                  # 1343,842 M/sec                  
         8 223 547      branch-misses             #    0,17% of all branches        

       3,601669402 seconds time elapsed
```

**valgrind tool=cachegrind**

```bash
==7866== Cachegrind, a cache and branch-prediction profiler
==7866== Copyright (C) 2002-2017, and GNU GPL'd, by Nicholas Nethercote et al.
==7866== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==7866== Command: bin/matmult_lab4.test
==7866== 
--7866-- warning: L3 cache found, using its data for the LL simulation.
Inicjowanie macierzy ...
Zainicjowano w 102.012413 (CPU: 101.433280)
Mnożenie macierz wektor macierzy gęstej
Pomnożono w 12.786121 (CPU: 12.782088) ... 0.000000
==7866== 
==7866== I   refs:      25,302,424,243
==7866== I1  misses:             1,383
==7866== LLi misses:             1,370
==7866== I1  miss rate:           0.00%
==7866== LLi miss rate:           0.00%
==7866== 
==7866== D   refs:      13,909,436,179  (10,706,496,379 rd   + 3,202,939,800 wr)
==7866== D1  misses:        75,456,361  (    44,053,680 rd   +    31,402,681 wr)
==7866== LLd misses:        62,508,041  (    31,254,676 rd   +    31,253,365 wr)
==7866== D1  miss rate:            0.5% (           0.4%     +           1.0%  )
==7866== LLd miss rate:            0.4% (           0.3%     +           1.0%  )
==7866== 
==7866== LL refs:           75,457,744  (    44,055,063 rd   +    31,402,681 wr)
==7866== LL misses:         62,509,411  (    31,256,046 rd   +    31,253,365 wr)
==7866== LL miss rate:             0.2% (           0.1%     +           1.0%  )
```

**Wyliczone dane**
| Dana | Wartość |
|---|:---:|
| Czas wykonania |0.3 s|
| Wydajność GFLOPS |1.31 GFlop/s|
| Liczba dost. do pam. |5095810728|
| Średni czas poj. dostępu |3e-10 s|
| Liczba trafień/chybień L1 |5075159613/25010997|
| Liczba trafień/chybień LL |12509549/12501473|

### 2. Rzadkie
----------

N = 1 000 000
NNZW = 100

**valgrind - samo gen**
==13837== Cachegrind, a cache and branch-prediction profiler
==13837== Copyright (C) 2002-2017, and GNU GPL'd, by Nicholas Nethercote et al.
==13837== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==13837== Command: bin/matmult_lab4.test
==13837== 
--13837-- warning: L3 cache found, using its data for the LL simulation.
Inicjowanie macierzy ...
Zainicjowano w 479.003383 (CPU: 478.316117)
==13837== 
==13837== I   refs:      107,500,269,673
==13837== I1  misses:              1,368
==13837== LLi misses:              1,355
==13837== I1  miss rate:            0.00%
==13837== LLi miss rate:            0.00%
==13837== 
==13837== D   refs:       62,390,720,226  (52,336,662,456 rd   + 10,054,057,770 wr)
==13837== D1  misses:         19,026,323  (        88,034 rd   +     18,938,289 wr)
==13837== LLd misses:         18,941,402  (         3,179 rd   +     18,938,223 wr)
==13837== D1  miss rate:             0.0% (           0.0%     +            0.2%  )
==13837== LLd miss rate:             0.0% (           0.0%     +            0.2%  )
==13837== 
==13837== LL refs:            19,027,691  (        89,402 rd   +     18,938,289 wr)
==13837== LL misses:          18,942,757  (         4,534 rd   +     18,938,223 wr)
==13837== LL miss rate:              0.0% (           0.0%     +            0.2%  )

**perf stat**
Inicjowanie macierzy ...
Zainicjowano w 21.400103 (CPU: 21.140484)

 Performance counter stats for 'bin/matmult_lab4.test':

      21415,425734      task-clock (msec)         #    0,998 CPUs utilized          
             1 913      context-switches          #    0,089 K/sec                  
                 3      cpu-migrations            #    0,000 K/sec                  
           295 959      page-faults               #    0,014 M/sec                  
    74 405 285 231      cycles                    #    3,474 GHz                    
   108 419 381 286      instructions              #    1,46  insn per cycle         
    20 274 477 314      branches                  #  946,723 M/sec                  
       416 702 467      branch-misses             #    2,06% of all branches        

      21,464068773 seconds time elapsed

### z mnożeniem

**valgrind**

==13462== Cachegrind, a cache and branch-prediction profiler
==13462== Copyright (C) 2002-2017, and GNU GPL'd, by Nicholas Nethercote et al.
==13462== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==13462== Command: bin/matmult_lab4.test
==13462== 
--13462-- warning: L3 cache found, using its data for the LL simulation.
Inicjowanie macierzy ...
Zainicjowano w 500.982018 (CPU: 499.678178)
Mnożenie macierz wektor macierzy rzadkiej
Pomnożono w 29.072380 (CPU: 29.056351) ... 2.798408
==13462== 
==13462== I   refs:      111,925,343,235
==13462== I1  misses:              1,387
==13462== LLi misses:              1,374
==13462== I1  miss rate:            0.00%
==13462== LLi miss rate:            0.00%
==13462== 
==13462== D   refs:       64,605,745,028  (54,450,684,993 rd   + 10,155,060,035 wr)
==13462== D1  misses:        136,767,288  (   117,828,956 rd   +     18,938,332 wr)
==13462== LLd misses:        105,485,011  (    86,546,745 rd   +     18,938,266 wr)
==13462== D1  miss rate:             0.2% (           0.2%     +            0.2%  )
==13462== LLd miss rate:             0.2% (           0.2%     +            0.2%  )
==13462== 
==13462== LL refs:           136,768,675  (   117,830,343 rd   +     18,938,332 wr)
==13462== LL misses:         105,486,385  (    86,548,119 rd   +     18,938,266 wr)
==13462== LL miss rate:              0.1% (           0.1%     +            0.2%  )

**perf stat**

Inicjowanie macierzy ...
Zainicjowano w 22.062263 (CPU: 21.877014)
Mnożenie macierz wektor macierzy rzadkiej
Pomnożono w 2.638115 (CPU: 2.633649) ... 2.798408

 Performance counter stats for 'bin/matmult_lab4.test':

      24770,439200      task-clock (msec)         #    1,000 CPUs utilized          
               135      context-switches          #    0,005 K/sec                  
                 3      cpu-migrations            #    0,000 K/sec                  
           299 864      page-faults               #    0,012 M/sec                  
    84 609 357 011      cycles                    #    3,416 GHz                    
   112 878 091 381      instructions              #    1,33  insn per cycle         
    20 384 200 385      branches                  #  822,924 M/sec                  
       417 493 709      branch-misses             #    2,05% of all branches        

      24,772056543 seconds time elapsed


**Wyliczone dane**
| Dana | Wartość |
|---|:---:|
| Czas wykonania |3.31 s|
| Wydajność GFLOPS |0.15|
| Liczba dost. do pam. |4*10e9|
| Średni czas poj. dostępu |6.8e-10|
| Liczba trafień/chybień L1 |6583668326/56430038|
| Liczba trafień/chybień LL |12863399/43566654|
