   1              		.file	"stream_d.c"
   2              		.text
   3              	.Ltext0:
   4              		.p2align 4,,15
   5              		.globl	checktick
   7              	checktick:
   8              	.LFB24:
   9              		.file 1 "stream_d.c"
   1:stream_d.c    **** # include <stdio.h>
   2:stream_d.c    **** # include <math.h>
   3:stream_d.c    **** # include <float.h>
   4:stream_d.c    **** # include <limits.h>
   5:stream_d.c    **** # include <sys/time.h>
   6:stream_d.c    **** 
   7:stream_d.c    **** /*
   8:stream_d.c    ****  * Program: Stream
   9:stream_d.c    ****  * Programmer: Joe R. Zagar
  10:stream_d.c    ****  * Revision: 4.0-BETA, October 24, 1995
  11:stream_d.c    ****  * Original code developed by John D. McCalpin
  12:stream_d.c    ****  *
  13:stream_d.c    ****  * This program measures memory transfer rates in MB/s for simple 
  14:stream_d.c    ****  * computational kernels coded in C.  These numbers reveal the quality
  15:stream_d.c    ****  * of code generation for simple uncacheable kernels as well as showing
  16:stream_d.c    ****  * the cost of floating-point operations relative to memory accesses.
  17:stream_d.c    ****  *
  18:stream_d.c    ****  * INSTRUCTIONS:
  19:stream_d.c    ****  *
  20:stream_d.c    ****  *	1) Stream requires a good bit of memory to run.  Adjust the
  21:stream_d.c    ****  *          value of 'N' (below) to give a 'timing calibration' of 
  22:stream_d.c    ****  *          at least 20 clock-ticks.  This will provide rate estimates
  23:stream_d.c    ****  *          that should be good to about 5% precision.
  24:stream_d.c    ****  */
  25:stream_d.c    **** 
  26:stream_d.c    **** # define N  100000000
  27:stream_d.c    **** # define NTIMES	10
  28:stream_d.c    **** # define OFFSET	0
  29:stream_d.c    **** 
  30:stream_d.c    **** /*
  31:stream_d.c    ****  *	3) Compile the code with full optimization.  Many compilers
  32:stream_d.c    ****  *	   generate unreasonably bad code before the optimizer tightens
  33:stream_d.c    ****  *	   things up.  If the results are unreasonably good, on the
  34:stream_d.c    ****  *	   other hand, the optimizer might be too smart for me!
  35:stream_d.c    ****  *
  36:stream_d.c    ****  *         Try compiling with:
  37:stream_d.c    ****  *               cc -O stream_d.c second.c -o stream_d -lm
  38:stream_d.c    ****  *
  39:stream_d.c    ****  *         This is known to work on Cray, SGI, IBM, and Sun machines.
  40:stream_d.c    ****  *
  41:stream_d.c    ****  *
  42:stream_d.c    ****  *	4) Mail the results to mccalpin@cs.virginia.edu
  43:stream_d.c    ****  *	   Be sure to include:
  44:stream_d.c    ****  *		a) computer hardware model number and software revision
  45:stream_d.c    ****  *		b) the compiler flags
  46:stream_d.c    ****  *		c) all of the output from the test case.
  47:stream_d.c    ****  * Thanks!
  48:stream_d.c    ****  *
  49:stream_d.c    ****  */
  50:stream_d.c    **** 
  51:stream_d.c    **** # define HLINE "-------------------------------------------------------------\n"
  52:stream_d.c    **** 
  53:stream_d.c    **** # ifndef MIN
  54:stream_d.c    **** # define MIN(x,y) ((x)<(y)?(x):(y))
  55:stream_d.c    **** # endif
  56:stream_d.c    **** # ifndef MAX
  57:stream_d.c    **** # define MAX(x,y) ((x)>(y)?(x):(y))
  58:stream_d.c    **** # endif
  59:stream_d.c    **** 
  60:stream_d.c    **** static double	a[N+OFFSET],
  61:stream_d.c    **** 		b[N+OFFSET],
  62:stream_d.c    **** 		c[N+OFFSET];
  63:stream_d.c    **** 
  64:stream_d.c    **** static double	rmstime[4] = {0}, maxtime[4] = {0},
  65:stream_d.c    **** 		mintime[4] = {FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX};
  66:stream_d.c    **** 
  67:stream_d.c    **** static char	*label[4] = {"Copy:      ", "Scale:     ",
  68:stream_d.c    ****     "Add:       ", "Triad:     "};
  69:stream_d.c    **** 
  70:stream_d.c    **** static double	bytes[4] = {
  71:stream_d.c    ****     2 * sizeof(double) * N,
  72:stream_d.c    ****     2 * sizeof(double) * N,
  73:stream_d.c    ****     3 * sizeof(double) * N,
  74:stream_d.c    ****     3 * sizeof(double) * N
  75:stream_d.c    ****     };
  76:stream_d.c    **** 
  77:stream_d.c    **** extern double second();
  78:stream_d.c    **** 
  79:stream_d.c    **** int
  80:stream_d.c    **** main()
  81:stream_d.c    ****     {
  82:stream_d.c    ****     int			quantum, checktick();
  83:stream_d.c    ****     int			BytesPerWord;
  84:stream_d.c    ****     register int	j, k;
  85:stream_d.c    ****     double		scalar, t, times[4][NTIMES];
  86:stream_d.c    **** 
  87:stream_d.c    ****     /* --- SETUP --- determine precision and check timing --- */
  88:stream_d.c    **** 
  89:stream_d.c    ****     printf(HLINE);
  90:stream_d.c    ****     BytesPerWord = sizeof(double);
  91:stream_d.c    ****     printf("This system uses %d bytes per DOUBLE PRECISION word.\n",
  92:stream_d.c    **** 	BytesPerWord);
  93:stream_d.c    **** 
  94:stream_d.c    ****     printf(HLINE);
  95:stream_d.c    ****     printf("Array size = %d, Offset = %d\n" , N, OFFSET);
  96:stream_d.c    ****     printf("Total memory required = %.1f MB.\n",
  97:stream_d.c    **** 	(3 * N * BytesPerWord) / 1048576.0);
  98:stream_d.c    ****     printf("Each test is run %d times, but only\n", NTIMES);
  99:stream_d.c    ****     printf("the *best* time for each is used.\n");
 100:stream_d.c    **** 
 101:stream_d.c    ****     /* Get initial value for system clock. */
 102:stream_d.c    **** 
 103:stream_d.c    ****     for (j=0; j<N; j++) {
 104:stream_d.c    **** 	a[j] = 1.0;
 105:stream_d.c    **** 	b[j] = 2.0;
 106:stream_d.c    **** 	c[j] = 0.0;
 107:stream_d.c    **** 	}
 108:stream_d.c    **** 
 109:stream_d.c    ****     printf(HLINE);
 110:stream_d.c    **** 
 111:stream_d.c    ****     if  ( (quantum = checktick()) >= 1) 
 112:stream_d.c    **** 	printf("Your clock granularity/precision appears to be "
 113:stream_d.c    **** 	    "%d microseconds.\n", quantum);
 114:stream_d.c    ****     else
 115:stream_d.c    **** 	printf("Your clock granularity appears to be "
 116:stream_d.c    **** 	    "less than one microsecond.\n");
 117:stream_d.c    **** 
 118:stream_d.c    ****     t = second();
 119:stream_d.c    ****     for (j = 0; j < N; j++)
 120:stream_d.c    **** 	a[j] = 2.0E0 * a[j];
 121:stream_d.c    ****     t = 1.0E6 * (second() - t);
 122:stream_d.c    **** 
 123:stream_d.c    ****     printf("Each test below will take on the order"
 124:stream_d.c    **** 	" of %d microseconds.\n", (int) t  );
 125:stream_d.c    ****     printf("   (= %d clock ticks)\n", (int) (t/quantum) );
 126:stream_d.c    ****     printf("Increase the size of the arrays if this shows that\n");
 127:stream_d.c    ****     printf("you are not getting at least 20 clock ticks per test.\n");
 128:stream_d.c    **** 
 129:stream_d.c    ****     printf(HLINE);
 130:stream_d.c    **** 
 131:stream_d.c    ****     printf("WARNING -- The above is only a rough guideline.\n");
 132:stream_d.c    ****     printf("For best results, please be sure you know the\n");
 133:stream_d.c    ****     printf("precision of your system timer.\n");
 134:stream_d.c    ****     printf(HLINE);
 135:stream_d.c    ****     
 136:stream_d.c    ****     /*	--- MAIN LOOP --- repeat test cases NTIMES times --- */
 137:stream_d.c    **** 
 138:stream_d.c    ****     scalar = 3.0;
 139:stream_d.c    ****     for (k=0; k<NTIMES; k++)
 140:stream_d.c    **** 	{
 141:stream_d.c    **** 	times[0][k] = second();
 142:stream_d.c    **** 	for (j=0; j<N; j++){
 143:stream_d.c    **** 	    c[j] = a[j];
 144:stream_d.c    **** 	}
 145:stream_d.c    **** 	times[0][k] = second() - times[0][k];
 146:stream_d.c    **** 	times[1][k] = second();
 147:stream_d.c    **** 	for (j=0; j<N; j++)
 148:stream_d.c    **** 	    b[j] = scalar*c[j];
 149:stream_d.c    **** 	times[1][k] = second() - times[1][k];
 150:stream_d.c    **** 	
 151:stream_d.c    **** 	times[2][k] = second();
 152:stream_d.c    **** 	for (j=0; j<N; j++)
 153:stream_d.c    **** 	    c[j] = a[j]+b[j];
 154:stream_d.c    **** 	times[2][k] = second() - times[2][k];
 155:stream_d.c    **** 	
 156:stream_d.c    **** 	times[3][k] = second();
 157:stream_d.c    **** 	for (j=0; j<N; j++)
 158:stream_d.c    **** 	    a[j] = b[j]+scalar*c[j];
 159:stream_d.c    **** 	times[3][k] = second() - times[3][k];
 160:stream_d.c    **** 	}
 161:stream_d.c    ****     
 162:stream_d.c    ****     /*	--- SUMMARY --- */
 163:stream_d.c    **** 
 164:stream_d.c    ****     for (k=0; k<NTIMES; k++)
 165:stream_d.c    **** 	{
 166:stream_d.c    **** 	for (j=0; j<4; j++)
 167:stream_d.c    **** 	    {
 168:stream_d.c    **** 	    rmstime[j] = rmstime[j] + (times[j][k] * times[j][k]);
 169:stream_d.c    **** 	    mintime[j] = MIN(mintime[j], times[j][k]);
 170:stream_d.c    **** 	    maxtime[j] = MAX(maxtime[j], times[j][k]);
 171:stream_d.c    **** 	    }
 172:stream_d.c    **** 	}
 173:stream_d.c    ****     
 174:stream_d.c    ****     printf("Function      Rate (MB/s)   RMS time     Min time     Max time\n");
 175:stream_d.c    ****     for (j=0; j<4; j++) {
 176:stream_d.c    **** 	rmstime[j] = sqrt(rmstime[j]/(double)NTIMES);
 177:stream_d.c    **** 
 178:stream_d.c    **** 	printf("%s%11.4f  %11.4f  %11.4f  %11.4f\n", label[j],
 179:stream_d.c    **** 	       1.0E-06 * bytes[j]/mintime[j],
 180:stream_d.c    **** 	       rmstime[j],
 181:stream_d.c    **** 	       mintime[j],
 182:stream_d.c    **** 	       maxtime[j]);
 183:stream_d.c    ****     }
 184:stream_d.c    ****     return 0;
 185:stream_d.c    **** }
 186:stream_d.c    **** 
 187:stream_d.c    **** # define	M	20
 188:stream_d.c    **** 
 189:stream_d.c    **** int
 190:stream_d.c    **** checktick()
 191:stream_d.c    ****     {
  10              		.loc 1 191 0
  11              		.cfi_startproc
  12 0000 55       		pushq	%rbp
  13              		.cfi_def_cfa_offset 16
  14              		.cfi_offset 6, -16
  15 0001 53       		pushq	%rbx
  16              		.cfi_def_cfa_offset 24
  17              		.cfi_offset 3, -24
  18 0002 4881ECC8 		subq	$200, %rsp
  18      000000
  19              		.cfi_def_cfa_offset 224
  20 0009 488D5C24 		leaq	16(%rsp), %rbx
  20      10
  21              		.loc 1 191 0
  22 000e 64488B04 		movq	%fs:40, %rax
  22      25280000 
  22      00
  23 0017 48898424 		movq	%rax, 184(%rsp)
  23      B8000000 
  24 001f 31C0     		xorl	%eax, %eax
  25              	.LVL0:
  26 0021 488DABA0 		leaq	160(%rbx), %rbp
  26      000000
  27              	.LVL1:
  28 0028 0F1F8400 		.p2align 4,,10
  28      00000000 
  29              		.p2align 3
  30              	.L3:
 192:stream_d.c    ****     int		i, minDelta, Delta;
 193:stream_d.c    ****     double	t1, t2, timesfound[M];
 194:stream_d.c    **** 
 195:stream_d.c    **** /*  Collect a sequence of M unique time values from the system. */
 196:stream_d.c    **** 
 197:stream_d.c    ****     for (i = 0; i < M; i++) {
 198:stream_d.c    **** 	t1 = second();
  31              		.loc 1 198 0
  32 0030 31C0     		xorl	%eax, %eax
  33 0032 E8000000 		call	second@PLT
  33      00
  34              	.LVL2:
  35 0037 F20F1144 		movsd	%xmm0, 8(%rsp)
  35      2408
  36              	.LVL3:
  37 003d 0F1F00   		.p2align 4,,10
  38              		.p2align 3
  39              	.L2:
 199:stream_d.c    **** 	while( ((t2=second()) - t1) < 1.0E-6 )
  40              		.loc 1 199 0 discriminator 1
  41 0040 31C0     		xorl	%eax, %eax
  42 0042 E8000000 		call	second@PLT
  42      00
  43              	.LVL4:
  44 0047 660F28C8 		movapd	%xmm0, %xmm1
  45 004b F20F103D 		movsd	.LC0(%rip), %xmm7
  45      00000000 
  46 0053 F20F5C4C 		subsd	8(%rsp), %xmm1
  46      2408
  47 0059 660F2EF9 		ucomisd	%xmm1, %xmm7
  48 005d 77E1     		ja	.L2
  49              	.LVL5:
 200:stream_d.c    **** 	    ;
 201:stream_d.c    **** 	timesfound[i] = t1 = t2;
  50              		.loc 1 201 0 discriminator 2
  51 005f F20F1103 		movsd	%xmm0, (%rbx)
  52 0063 4883C308 		addq	$8, %rbx
 197:stream_d.c    **** 	t1 = second();
  53              		.loc 1 197 0 discriminator 2
  54 0067 4839EB   		cmpq	%rbp, %rbx
  55 006a 75C4     		jne	.L3
  56              	.LVL6:
 202:stream_d.c    **** 	}
 203:stream_d.c    **** 
 204:stream_d.c    **** /*
 205:stream_d.c    ****  * Determine the minimum difference between these M values.
 206:stream_d.c    ****  * This result will be our estimate (in microseconds) for the
 207:stream_d.c    ****  * clock granularity.
 208:stream_d.c    ****  */
 209:stream_d.c    **** 
 210:stream_d.c    ****     minDelta = 1000000;
 211:stream_d.c    ****     for (i = 1; i < M; i++) {
 212:stream_d.c    **** 	Delta = (int)( 1.0E6 * (timesfound[i]-timesfound[i-1]));
  57              		.loc 1 212 0
  58 006c 660F1044 		movupd	24(%rsp), %xmm0
  58      2418
  59              	.LVL7:
 213:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
  60              		.loc 1 213 0
  61 0072 31C9     		xorl	%ecx, %ecx
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
  62              		.loc 1 212 0
  63 0074 660F104C 		movupd	40(%rsp), %xmm1
  63      2428
  64 007a 660F5C44 		subpd	16(%rsp), %xmm0
  64      2410
  65 0080 660F2815 		movapd	.LC1(%rip), %xmm2
  65      00000000 
  66 0088 660F5C4C 		subpd	32(%rsp), %xmm1
  66      2420
  67 008e 660F59C2 		mulpd	%xmm2, %xmm0
  68 0092 660F59CA 		mulpd	%xmm2, %xmm1
  69 0096 660FE6C0 		cvttpd2dq	%xmm0, %xmm0
  70 009a 660FE6C9 		cvttpd2dq	%xmm1, %xmm1
  71 009e 660F6CC1 		punpcklqdq	%xmm1, %xmm0
  72              		.loc 1 213 0
  73 00a2 660FEFC9 		pxor	%xmm1, %xmm1
  74 00a6 660F6FD8 		movdqa	%xmm0, %xmm3
  75 00aa 660F66D9 		pcmpgtd	%xmm1, %xmm3
  76 00ae 660FDBC3 		pand	%xmm3, %xmm0
  77 00b2 660F6F1D 		movdqa	.LC2(%rip), %xmm3
  77      00000000 
  78 00ba 660F6FE0 		movdqa	%xmm0, %xmm4
  79 00be 660F6FEB 		movdqa	%xmm3, %xmm5
  80 00c2 660F66E3 		pcmpgtd	%xmm3, %xmm4
  81 00c6 660F6FDC 		movdqa	%xmm4, %xmm3
  82 00ca 660FDBEC 		pand	%xmm4, %xmm5
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
  83              		.loc 1 212 0
  84 00ce 660F1064 		movupd	72(%rsp), %xmm4
  84      2448
  85              		.loc 1 213 0
  86 00d4 660FDFD8 		pandn	%xmm0, %xmm3
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
  87              		.loc 1 212 0
  88 00d8 660F1044 		movupd	56(%rsp), %xmm0
  88      2438
  89 00de 660F5C64 		subpd	64(%rsp), %xmm4
  89      2440
  90              		.loc 1 213 0
  91 00e4 660FEBDD 		por	%xmm5, %xmm3
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
  92              		.loc 1 212 0
  93 00e8 660F5C44 		subpd	48(%rsp), %xmm0
  93      2430
  94              		.loc 1 213 0
  95 00ee 660F6FEB 		movdqa	%xmm3, %xmm5
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
  96              		.loc 1 212 0
  97 00f2 660F59E2 		mulpd	%xmm2, %xmm4
  98 00f6 660F59C2 		mulpd	%xmm2, %xmm0
  99 00fa 660FE6E4 		cvttpd2dq	%xmm4, %xmm4
 100 00fe 660FE6C0 		cvttpd2dq	%xmm0, %xmm0
 101 0102 660F6CC4 		punpcklqdq	%xmm4, %xmm0
 102              		.loc 1 213 0
 103 0106 660F6FE0 		movdqa	%xmm0, %xmm4
 104 010a 660F66E1 		pcmpgtd	%xmm1, %xmm4
 105 010e 660FDBC4 		pand	%xmm4, %xmm0
 106 0112 660F6FE0 		movdqa	%xmm0, %xmm4
 107 0116 660F66E3 		pcmpgtd	%xmm3, %xmm4
 108 011a 660F6FDC 		movdqa	%xmm4, %xmm3
 109 011e 660FDBEC 		pand	%xmm4, %xmm5
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 110              		.loc 1 212 0
 111 0122 660F1064 		movupd	104(%rsp), %xmm4
 111      2468
 112              		.loc 1 213 0
 113 0128 660FDFD8 		pandn	%xmm0, %xmm3
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 114              		.loc 1 212 0
 115 012c 660F1044 		movupd	88(%rsp), %xmm0
 115      2458
 116 0132 660F5C64 		subpd	96(%rsp), %xmm4
 116      2460
 117              		.loc 1 213 0
 118 0138 660FEBDD 		por	%xmm5, %xmm3
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 119              		.loc 1 212 0
 120 013c 660F5C44 		subpd	80(%rsp), %xmm0
 120      2450
 121 0142 660F59E2 		mulpd	%xmm2, %xmm4
 122 0146 660F59C2 		mulpd	%xmm2, %xmm0
 123 014a 660FE6E4 		cvttpd2dq	%xmm4, %xmm4
 124 014e 660FE6C0 		cvttpd2dq	%xmm0, %xmm0
 125 0152 660F6CC4 		punpcklqdq	%xmm4, %xmm0
 126              		.loc 1 213 0
 127 0156 660F6FE0 		movdqa	%xmm0, %xmm4
 128 015a 660F66E1 		pcmpgtd	%xmm1, %xmm4
 129 015e 660FDBC4 		pand	%xmm4, %xmm0
 130 0162 660F6FE0 		movdqa	%xmm0, %xmm4
 131 0166 660F66E3 		pcmpgtd	%xmm3, %xmm4
 132 016a 660FDBDC 		pand	%xmm4, %xmm3
 133 016e 660FDFE0 		pandn	%xmm0, %xmm4
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 134              		.loc 1 212 0
 135 0172 660F1044 		movupd	120(%rsp), %xmm0
 135      2478
 136 0178 660F5C44 		subpd	112(%rsp), %xmm0
 136      2470
 137              		.loc 1 213 0
 138 017e 660FEBDC 		por	%xmm4, %xmm3
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 139              		.loc 1 212 0
 140 0182 660F10A4 		movupd	136(%rsp), %xmm4
 140      24880000 
 140      00
 141 018b 660F5CA4 		subpd	128(%rsp), %xmm4
 141      24800000 
 141      00
 142 0194 660F59C2 		mulpd	%xmm2, %xmm0
 143 0198 660F59D4 		mulpd	%xmm4, %xmm2
 144 019c 660FE6C0 		cvttpd2dq	%xmm0, %xmm0
 145 01a0 F20F1025 		movsd	.LC3(%rip), %xmm4
 145      00000000 
 146 01a8 660FE6D2 		cvttpd2dq	%xmm2, %xmm2
 147 01ac 660F6CC2 		punpcklqdq	%xmm2, %xmm0
 148              		.loc 1 213 0
 149 01b0 660F6FE8 		movdqa	%xmm0, %xmm5
 150 01b4 660F66E9 		pcmpgtd	%xmm1, %xmm5
 151 01b8 660FDBC5 		pand	%xmm5, %xmm0
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 152              		.loc 1 212 0
 153 01bc F20F10AC 		movsd	160(%rsp), %xmm5
 153      24A00000 
 153      00
 154              		.loc 1 213 0
 155 01c5 660F6FC8 		movdqa	%xmm0, %xmm1
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 156              		.loc 1 212 0
 157 01c9 660F28F5 		movapd	%xmm5, %xmm6
 158              		.loc 1 213 0
 159 01cd 660F66CB 		pcmpgtd	%xmm3, %xmm1
 160 01d1 660FDBD9 		pand	%xmm1, %xmm3
 161 01d5 660FDFC8 		pandn	%xmm0, %xmm1
 162 01d9 660FEBD9 		por	%xmm1, %xmm3
 163              	.LVL8:
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 164              		.loc 1 212 0
 165 01dd F20F108C 		movsd	152(%rsp), %xmm1
 165      24980000 
 165      00
 166 01e6 660F6FD3 		movdqa	%xmm3, %xmm2
 167 01ea 660F6FC3 		movdqa	%xmm3, %xmm0
 168 01ee 660F73DA 		psrldq	$8, %xmm2
 168      08
 169 01f3 F20F5CF1 		subsd	%xmm1, %xmm6
 170 01f7 660F66C2 		pcmpgtd	%xmm2, %xmm0
 171 01fb 660FDBD0 		pand	%xmm0, %xmm2
 172 01ff 660FDFC3 		pandn	%xmm3, %xmm0
 173 0203 660FEBD0 		por	%xmm0, %xmm2
 174 0207 660F28C1 		movapd	%xmm1, %xmm0
 175 020b 660F28CE 		movapd	%xmm6, %xmm1
 176 020f F20F5C84 		subsd	144(%rsp), %xmm0
 176      24900000 
 176      00
 177 0218 F20F59CC 		mulsd	%xmm4, %xmm1
 178 021c 660F6FDA 		movdqa	%xmm2, %xmm3
 179 0220 660F73DB 		psrldq	$4, %xmm3
 179      04
 180              	.LVL9:
 181 0225 F20F2CD1 		cvttsd2si	%xmm1, %edx
 182 0229 F20F59C4 		mulsd	%xmm4, %xmm0
 183 022d F20F2CC0 		cvttsd2si	%xmm0, %eax
 184 0231 F20F1084 		movsd	168(%rsp), %xmm0
 184      24A80000 
 184      00
 185 023a F20F5CC5 		subsd	%xmm5, %xmm0
 186 023e F20F59C4 		mulsd	%xmm4, %xmm0
 187              		.loc 1 213 0
 188 0242 85C0     		testl	%eax, %eax
 189 0244 0F48C1   		cmovs	%ecx, %eax
 190 0247 85D2     		testl	%edx, %edx
 191 0249 0F48D1   		cmovs	%ecx, %edx
 192 024c 39D0     		cmpl	%edx, %eax
 193 024e 0F4FC2   		cmovg	%edx, %eax
 212:stream_d.c    **** 	minDelta = MIN(minDelta, MAX(Delta,0));
 194              		.loc 1 212 0
 195 0251 F20F2CD0 		cvttsd2si	%xmm0, %edx
 196              		.loc 1 213 0
 197 0255 660F6FC2 		movdqa	%xmm2, %xmm0
 198 0259 660F66C3 		pcmpgtd	%xmm3, %xmm0
 199 025d 660FDBD8 		pand	%xmm0, %xmm3
 200 0261 660FDFC2 		pandn	%xmm2, %xmm0
 201 0265 660FEBC3 		por	%xmm3, %xmm0
 202 0269 85D2     		testl	%edx, %edx
 203 026b 0F48D1   		cmovs	%ecx, %edx
 204 026e 39D0     		cmpl	%edx, %eax
 205 0270 0F4FC2   		cmovg	%edx, %eax
 206 0273 660F7EC2 		movd	%xmm0, %edx
 207 0277 39D0     		cmpl	%edx, %eax
 208 0279 0F4FC2   		cmovg	%edx, %eax
 209              	.LVL10:
 214:stream_d.c    **** 	}
 215:stream_d.c    **** 
 216:stream_d.c    ****     return(minDelta);
 217:stream_d.c    ****     }
 210              		.loc 1 217 0
 211 027c 488BB424 		movq	184(%rsp), %rsi
 211      B8000000 
 212 0284 64483334 		xorq	%fs:40, %rsi
 212      25280000 
 212      00
 213 028d 750A     		jne	.L9
 214 028f 4881C4C8 		addq	$200, %rsp
 214      000000
 215              		.cfi_remember_state
 216              		.cfi_def_cfa_offset 24
 217 0296 5B       		popq	%rbx
 218              		.cfi_def_cfa_offset 16
 219              	.LVL11:
 220 0297 5D       		popq	%rbp
 221              		.cfi_def_cfa_offset 8
 222 0298 C3       		ret
 223              	.LVL12:
 224              	.L9:
 225              		.cfi_restore_state
 226 0299 E8000000 		call	__stack_chk_fail@PLT
 226      00
 227              	.LVL13:
 228              		.cfi_endproc
 229              	.LFE24:
 231              		.section	.rodata.str1.8,"aMS",@progbits,1
 232              		.align 8
 233              	.LC4:
 234 0000 2D2D2D2D 		.string	"-------------------------------------------------------------"
 234      2D2D2D2D 
 234      2D2D2D2D 
 234      2D2D2D2D 
 234      2D2D2D2D 
 235 003e 0000     		.align 8
 236              	.LC5:
 237 0040 54686973 		.string	"This system uses %d bytes per DOUBLE PRECISION word.\n"
 237      20737973 
 237      74656D20 
 237      75736573 
 237      20256420 
 238              		.section	.rodata.str1.1,"aMS",@progbits,1
 239              	.LC6:
 240 0000 41727261 		.string	"Array size = %d, Offset = %d\n"
 240      79207369 
 240      7A65203D 
 240      2025642C 
 240      204F6666 
 241              		.section	.rodata.str1.8
 242 0076 0000     		.align 8
 243              	.LC8:
 244 0078 546F7461 		.string	"Total memory required = %.1f MB.\n"
 244      6C206D65 
 244      6D6F7279 
 244      20726571 
 244      75697265 
 245 009a 00000000 		.align 8
 245      0000
 246              	.LC9:
 247 00a0 45616368 		.string	"Each test is run %d times, but only\n"
 247      20746573 
 247      74206973 
 247      2072756E 
 247      20256420 
 248 00c5 000000   		.align 8
 249              	.LC10:
 250 00c8 74686520 		.string	"the *best* time for each is used."
 250      2A626573 
 250      742A2074 
 250      696D6520 
 250      666F7220 
 251 00ea 00000000 		.align 8
 251      0000
 252              	.LC13:
 253 00f0 596F7572 		.string	"Your clock granularity/precision appears to be %d microseconds.\n"
 253      20636C6F 
 253      636B2067 
 253      72616E75 
 253      6C617269 
 254 0131 00000000 		.align 8
 254      000000
 255              	.LC14:
 256 0138 596F7572 		.string	"Your clock granularity appears to be less than one microsecond."
 256      20636C6F 
 256      636B2067 
 256      72616E75 
 256      6C617269 
 257              		.align 8
 258              	.LC15:
 259 0178 45616368 		.string	"Each test below will take on the order of %d microseconds.\n"
 259      20746573 
 259      74206265 
 259      6C6F7720 
 259      77696C6C 
 260              		.section	.rodata.str1.1
 261              	.LC16:
 262 001e 20202028 		.string	"   (= %d clock ticks)\n"
 262      3D202564 
 262      20636C6F 
 262      636B2074 
 262      69636B73 
 263              		.section	.rodata.str1.8
 264 01b4 00000000 		.align 8
 265              	.LC17:
 266 01b8 496E6372 		.string	"Increase the size of the arrays if this shows that"
 266      65617365 
 266      20746865 
 266      2073697A 
 266      65206F66 
 267 01eb 00000000 		.align 8
 267      00
 268              	.LC18:
 269 01f0 796F7520 		.string	"you are not getting at least 20 clock ticks per test."
 269      61726520 
 269      6E6F7420 
 269      67657474 
 269      696E6720 
 270 0226 0000     		.align 8
 271              	.LC19:
 272 0228 5741524E 		.string	"WARNING -- The above is only a rough guideline."
 272      494E4720 
 272      2D2D2054 
 272      68652061 
 272      626F7665 
 273              		.align 8
 274              	.LC20:
 275 0258 466F7220 		.string	"For best results, please be sure you know the"
 275      62657374 
 275      20726573 
 275      756C7473 
 275      2C20706C 
 276 0286 0000     		.align 8
 277              	.LC21:
 278 0288 70726563 		.string	"precision of your system timer."
 278      6973696F 
 278      6E206F66 
 278      20796F75 
 278      72207379 
 279              		.align 8
 280              	.LC23:
 281 02a8 46756E63 		.string	"Function      Rate (MB/s)   RMS time     Min time     Max time"
 281      74696F6E 
 281      20202020 
 281      20205261 
 281      74652028 
 282 02e7 00       		.align 8
 283              	.LC26:
 284 02e8 25732531 		.string	"%s%11.4f  %11.4f  %11.4f  %11.4f\n"
 284      312E3466 
 284      20202531 
 284      312E3466 
 284      20202531 
 285              		.section	.text.startup,"ax",@progbits
 286              		.p2align 4,,15
 287              		.globl	main
 289              	main:
 290              	.LFB23:
  81:stream_d.c    ****     int			quantum, checktick();
 291              		.loc 1 81 0
 292              		.cfi_startproc
 293 0000 4157     		pushq	%r15
 294              		.cfi_def_cfa_offset 16
 295              		.cfi_offset 15, -16
 296 0002 4156     		pushq	%r14
 297              		.cfi_def_cfa_offset 24
 298              		.cfi_offset 14, -24
 299              	.LBB44:
 300              	.LBB45:
 301              		.file 2 "/usr/include/x86_64-linux-gnu/bits/stdio2.h"
   1:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** /* Checking macros for stdio functions.
   2:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    Copyright (C) 2004-2018 Free Software Foundation, Inc.
   3:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    This file is part of the GNU C Library.
   4:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
   5:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    The GNU C Library is free software; you can redistribute it and/or
   6:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    modify it under the terms of the GNU Lesser General Public
   7:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    License as published by the Free Software Foundation; either
   8:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    version 2.1 of the License, or (at your option) any later version.
   9:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  10:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    The GNU C Library is distributed in the hope that it will be useful,
  11:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    but WITHOUT ANY WARRANTY; without even the implied warranty of
  12:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  13:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    Lesser General Public License for more details.
  14:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  15:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    You should have received a copy of the GNU Lesser General Public
  16:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    License along with the GNU C Library; if not, see
  17:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****    <http://www.gnu.org/licenses/>.  */
  18:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  19:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #ifndef _STDIO_H
  20:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** # error "Never include <bits/stdio2.h> directly; use <stdio.h> instead."
  21:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #endif
  22:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  23:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __sprintf_chk (char *__restrict __s, int __flag, size_t __slen,
  24:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			  const char *__restrict __format, ...) __THROW;
  25:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __vsprintf_chk (char *__restrict __s, int __flag, size_t __slen,
  26:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			   const char *__restrict __format,
  27:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			   _G_va_list __ap) __THROW;
  28:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  29:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #ifdef __va_arg_pack
  30:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __fortify_function int
  31:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __NTH (sprintf (char *__restrict __s, const char *__restrict __fmt, ...))
  32:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** {
  33:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   return __builtin___sprintf_chk (__s, __USE_FORTIFY_LEVEL - 1,
  34:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 				  __bos (__s), __fmt, __va_arg_pack ());
  35:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** }
  36:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #elif !defined __cplusplus
  37:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** # define sprintf(str, ...) \
  38:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   __builtin___sprintf_chk (str, __USE_FORTIFY_LEVEL - 1, __bos (str), \
  39:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			   __VA_ARGS__)
  40:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #endif
  41:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  42:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __fortify_function int
  43:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __NTH (vsprintf (char *__restrict __s, const char *__restrict __fmt,
  44:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 		 _G_va_list __ap))
  45:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** {
  46:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   return __builtin___vsprintf_chk (__s, __USE_FORTIFY_LEVEL - 1,
  47:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 				   __bos (__s), __fmt, __ap);
  48:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** }
  49:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  50:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #if defined __USE_ISOC99 || defined __USE_UNIX98
  51:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  52:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __snprintf_chk (char *__restrict __s, size_t __n, int __flag,
  53:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			   size_t __slen, const char *__restrict __format,
  54:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			   ...) __THROW;
  55:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __vsnprintf_chk (char *__restrict __s, size_t __n, int __flag,
  56:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			    size_t __slen, const char *__restrict __format,
  57:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			    _G_va_list __ap) __THROW;
  58:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  59:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** # ifdef __va_arg_pack
  60:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __fortify_function int
  61:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __NTH (snprintf (char *__restrict __s, size_t __n,
  62:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 		 const char *__restrict __fmt, ...))
  63:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** {
  64:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   return __builtin___snprintf_chk (__s, __n, __USE_FORTIFY_LEVEL - 1,
  65:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 				   __bos (__s), __fmt, __va_arg_pack ());
  66:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** }
  67:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** # elif !defined __cplusplus
  68:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #  define snprintf(str, len, ...) \
  69:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   __builtin___snprintf_chk (str, len, __USE_FORTIFY_LEVEL - 1, __bos (str), \
  70:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			    __VA_ARGS__)
  71:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** # endif
  72:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  73:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __fortify_function int
  74:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __NTH (vsnprintf (char *__restrict __s, size_t __n,
  75:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 		  const char *__restrict __fmt, _G_va_list __ap))
  76:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** {
  77:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   return __builtin___vsnprintf_chk (__s, __n, __USE_FORTIFY_LEVEL - 1,
  78:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 				    __bos (__s), __fmt, __ap);
  79:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** }
  80:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  81:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #endif
  82:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  83:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** #if __USE_FORTIFY_LEVEL > 1
  84:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  85:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __fprintf_chk (FILE *__restrict __stream, int __flag,
  86:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			  const char *__restrict __format, ...);
  87:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __printf_chk (int __flag, const char *__restrict __format, ...);
  88:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __vfprintf_chk (FILE *__restrict __stream, int __flag,
  89:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			   const char *__restrict __format, _G_va_list __ap);
  90:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** extern int __vprintf_chk (int __flag, const char *__restrict __format,
  91:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			  _G_va_list __ap);
  92:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
  93:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** # ifdef __va_arg_pack
  94:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __fortify_function int
  95:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** fprintf (FILE *__restrict __stream, const char *__restrict __fmt, ...)
  96:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** {
  97:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   return __fprintf_chk (__stream, __USE_FORTIFY_LEVEL - 1, __fmt,
  98:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 			__va_arg_pack ());
  99:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** }
 100:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** 
 101:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** __fortify_function int
 102:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** printf (const char *__restrict __fmt, ...)
 103:/usr/include/x86_64-linux-gnu/bits/stdio2.h **** {
 104:/usr/include/x86_64-linux-gnu/bits/stdio2.h ****   return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
 302              		.loc 2 104 0
 303 0004 488D3D00 		leaq	.LC4(%rip), %rdi
 303      000000
 304              	.LBE45:
 305              	.LBE44:
  81:stream_d.c    ****     int			quantum, checktick();
 306              		.loc 1 81 0
 307 000b 4155     		pushq	%r13
 308              		.cfi_def_cfa_offset 32
 309              		.cfi_offset 13, -32
 310 000d 4154     		pushq	%r12
 311              		.cfi_def_cfa_offset 40
 312              		.cfi_offset 12, -40
 313 000f 4C8D3D00 		leaq	a(%rip), %r15
 313      000000
 314 0016 55       		pushq	%rbp
 315              		.cfi_def_cfa_offset 48
 316              		.cfi_offset 6, -48
 317 0017 53       		pushq	%rbx
 318              		.cfi_def_cfa_offset 56
 319              		.cfi_offset 3, -56
 320 0018 4C8D2D00 		leaq	b(%rip), %r13
 320      000000
 321 001f 4881EC78 		subq	$376, %rsp
 321      010000
 322              		.cfi_def_cfa_offset 432
  81:stream_d.c    ****     int			quantum, checktick();
 323              		.loc 1 81 0
 324 0026 64488B04 		movq	%fs:40, %rax
 324      25280000 
 324      00
 325 002f 48898424 		movq	%rax, 360(%rsp)
 325      68010000 
 326 0037 31C0     		xorl	%eax, %eax
 327              	.LVL14:
 328              	.LBB47:
 329              	.LBB46:
 330              		.loc 2 104 0
 331 0039 E8000000 		call	puts@PLT
 331      00
 332              	.LVL15:
 333              	.LBE46:
 334              	.LBE47:
 335              	.LBB48:
 336              	.LBB49:
 337 003e 488D3500 		leaq	.LC5(%rip), %rsi
 337      000000
 338 0045 BA080000 		movl	$8, %edx
 338      00
 339 004a BF010000 		movl	$1, %edi
 339      00
 340 004f 31C0     		xorl	%eax, %eax
 341 0051 E8000000 		call	__printf_chk@PLT
 341      00
 342              	.LVL16:
 343              	.LBE49:
 344              	.LBE48:
 345              	.LBB50:
 346              	.LBB51:
 347 0056 488D3D00 		leaq	.LC4(%rip), %rdi
 347      000000
 348 005d E8000000 		call	puts@PLT
 348      00
 349              	.LVL17:
 350              	.LBE51:
 351              	.LBE50:
 352              	.LBB52:
 353              	.LBB53:
 354 0062 488D3500 		leaq	.LC6(%rip), %rsi
 354      000000
 355 0069 31C9     		xorl	%ecx, %ecx
 356 006b BA00E1F5 		movl	$100000000, %edx
 356      05
 357 0070 BF010000 		movl	$1, %edi
 357      00
 358 0075 31C0     		xorl	%eax, %eax
 359 0077 E8000000 		call	__printf_chk@PLT
 359      00
 360              	.LVL18:
 361              	.LBE53:
 362              	.LBE52:
 363              	.LBB54:
 364              	.LBB55:
 365 007c F20F1005 		movsd	.LC7(%rip), %xmm0
 365      00000000 
 366 0084 488D3500 		leaq	.LC8(%rip), %rsi
 366      000000
 367 008b BF010000 		movl	$1, %edi
 367      00
 368 0090 B8010000 		movl	$1, %eax
 368      00
 369 0095 E8000000 		call	__printf_chk@PLT
 369      00
 370              	.LVL19:
 371              	.LBE55:
 372              	.LBE54:
 373              	.LBB56:
 374              	.LBB57:
 375 009a 488D3500 		leaq	.LC9(%rip), %rsi
 375      000000
 376 00a1 BA0A0000 		movl	$10, %edx
 376      00
 377 00a6 BF010000 		movl	$1, %edi
 377      00
 378 00ab 31C0     		xorl	%eax, %eax
 379 00ad E8000000 		call	__printf_chk@PLT
 379      00
 380              	.LVL20:
 381              	.LBE57:
 382              	.LBE56:
 383              	.LBB58:
 384              	.LBB59:
 385 00b2 488D3D00 		leaq	.LC10(%rip), %rdi
 385      000000
 386 00b9 E8000000 		call	puts@PLT
 386      00
 387              	.LVL21:
 388 00be 660F280D 		movapd	.LC11(%rip), %xmm1
 388      00000000 
 389 00c6 31C0     		xorl	%eax, %eax
 390 00c8 660F2805 		movapd	.LC12(%rip), %xmm0
 390      00000000 
 391              	.LVL22:
 392              		.p2align 4,,10
 393              		.p2align 3
 394              	.L11:
 395              	.LBE59:
 396              	.LBE58:
 104:stream_d.c    **** 	b[j] = 2.0;
 397              		.loc 1 104 0
 398 00d0 410F290C 		movaps	%xmm1, (%r15,%rax)
 398      07
 105:stream_d.c    **** 	c[j] = 0.0;
 399              		.loc 1 105 0
 400 00d5 410F2944 		movaps	%xmm0, 0(%r13,%rax)
 400      0500
 401 00db 4883C010 		addq	$16, %rax
 402 00df 483D0008 		cmpq	$800000000, %rax
 402      AF2F
 403 00e5 75E9     		jne	.L11
 404 00e7 488D3D00 		leaq	c(%rip), %rdi
 404      000000
 405 00ee 31F6     		xorl	%esi, %esi
 406 00f0 BA0008AF 		movl	$800000000, %edx
 406      2F
 407 00f5 E8000000 		call	memset@PLT
 407      00
 408              	.LVL23:
 409              	.LBB60:
 410              	.LBB61:
 411              		.loc 2 104 0
 412 00fa 488D3D00 		leaq	.LC4(%rip), %rdi
 412      000000
 413 0101 E8000000 		call	puts@PLT
 413      00
 414              	.LVL24:
 415              	.LBE61:
 416              	.LBE60:
 111:stream_d.c    **** 	printf("Your clock granularity/precision appears to be "
 417              		.loc 1 111 0
 418 0106 31C0     		xorl	%eax, %eax
 419 0108 E8000000 		call	checktick
 419      00
 420              	.LVL25:
 421 010d 85C0     		testl	%eax, %eax
 422 010f 89C3     		movl	%eax, %ebx
 423              	.LVL26:
 424 0111 0F8E4F04 		jle	.L12
 424      0000
 425              	.LVL27:
 426              	.LBB62:
 427              	.LBB63:
 428              		.loc 2 104 0
 429 0117 488D3500 		leaq	.LC13(%rip), %rsi
 429      000000
 430 011e 89C2     		movl	%eax, %edx
 431 0120 BF010000 		movl	$1, %edi
 431      00
 432 0125 31C0     		xorl	%eax, %eax
 433              	.LVL28:
 434 0127 E8000000 		call	__printf_chk@PLT
 434      00
 435              	.LVL29:
 436              	.L13:
 437              	.LBE63:
 438              	.LBE62:
 118:stream_d.c    ****     for (j = 0; j < N; j++)
 439              		.loc 1 118 0
 440 012c 31C0     		xorl	%eax, %eax
 441 012e E8000000 		call	second@PLT
 441      00
 442              	.LVL30:
 443 0133 488D0500 		leaq	a(%rip), %rax
 443      000000
 444 013a F20F1144 		movsd	%xmm0, 8(%rsp)
 444      2408
 445              	.LVL31:
 446 0140 498D9700 		leaq	800000000(%r15), %rdx
 446      08AF2F
 447              	.LVL32:
 448 0147 660F1F84 		.p2align 4,,10
 448      00000000 
 448      00
 449              		.p2align 3
 450              	.L14:
 120:stream_d.c    ****     t = 1.0E6 * (second() - t);
 451              		.loc 1 120 0 discriminator 3
 452 0150 660F2800 		movapd	(%rax), %xmm0
 453 0154 4883C010 		addq	$16, %rax
 454 0158 660F58C0 		addpd	%xmm0, %xmm0
 455 015c 0F2940F0 		movaps	%xmm0, -16(%rax)
 456 0160 4839D0   		cmpq	%rdx, %rax
 457 0163 75EB     		jne	.L14
 121:stream_d.c    **** 
 458              		.loc 1 121 0
 459 0165 31C0     		xorl	%eax, %eax
 460 0167 488D6C24 		leaq	32(%rsp), %rbp
 460      20
 461 016c 4C8D3500 		leaq	c(%rip), %r14
 461      000000
 462 0173 E8000000 		call	second@PLT
 462      00
 463              	.LVL33:
 464 0178 F20F5C44 		subsd	8(%rsp), %xmm0
 464      2408
 465              	.LBB64:
 466              	.LBB65:
 467              		.loc 2 104 0
 468 017e 488D3500 		leaq	.LC15(%rip), %rsi
 468      000000
 469 0185 BF010000 		movl	$1, %edi
 469      00
 470 018a 31C0     		xorl	%eax, %eax
 471 018c 4C8D6550 		leaq	80(%rbp), %r12
 472              	.LBE65:
 473              	.LBE64:
 121:stream_d.c    **** 
 474              		.loc 1 121 0
 475 0190 F20F5905 		mulsd	.LC3(%rip), %xmm0
 475      00000000 
 476              	.LVL34:
 123:stream_d.c    **** 	" of %d microseconds.\n", (int) t  );
 477              		.loc 1 123 0
 478 0198 F20F2CD0 		cvttsd2si	%xmm0, %edx
 479 019c F20F1144 		movsd	%xmm0, 8(%rsp)
 479      2408
 480              	.LBB67:
 481              	.LBB66:
 482              		.loc 2 104 0
 483 01a2 E8000000 		call	__printf_chk@PLT
 483      00
 484              	.LVL35:
 485              	.LBE66:
 486              	.LBE67:
 125:stream_d.c    ****     printf("Increase the size of the arrays if this shows that\n");
 487              		.loc 1 125 0
 488 01a7 660FEFC9 		pxor	%xmm1, %xmm1
 489              	.LBB68:
 490              	.LBB69:
 491              		.loc 2 104 0
 492 01ab 488D3500 		leaq	.LC16(%rip), %rsi
 492      000000
 493              	.LBE69:
 494              	.LBE68:
 125:stream_d.c    ****     printf("Increase the size of the arrays if this shows that\n");
 495              		.loc 1 125 0
 496 01b2 F20F1044 		movsd	8(%rsp), %xmm0
 496      2408
 497              	.LBB72:
 498              	.LBB70:
 499              		.loc 2 104 0
 500 01b8 BF010000 		movl	$1, %edi
 500      00
 501 01bd 31C0     		xorl	%eax, %eax
 502              	.LBE70:
 503              	.LBE72:
 125:stream_d.c    ****     printf("Increase the size of the arrays if this shows that\n");
 504              		.loc 1 125 0
 505 01bf F20F2ACB 		cvtsi2sd	%ebx, %xmm1
 506              	.LBB73:
 507              	.LBB74:
 508              		.loc 2 104 0
 509 01c3 4889EB   		movq	%rbp, %rbx
 510              	.LVL36:
 511              	.LBE74:
 512              	.LBE73:
 125:stream_d.c    ****     printf("Increase the size of the arrays if this shows that\n");
 513              		.loc 1 125 0
 514 01c6 F20F5EC1 		divsd	%xmm1, %xmm0
 515 01ca F20F2CD0 		cvttsd2si	%xmm0, %edx
 516              	.LBB76:
 517              	.LBB71:
 518              		.loc 2 104 0
 519 01ce E8000000 		call	__printf_chk@PLT
 519      00
 520              	.LVL37:
 521              	.LBE71:
 522              	.LBE76:
 523              	.LBB77:
 524              	.LBB78:
 525 01d3 488D3D00 		leaq	.LC17(%rip), %rdi
 525      000000
 526 01da E8000000 		call	puts@PLT
 526      00
 527              	.LVL38:
 528              	.LBE78:
 529              	.LBE77:
 530              	.LBB79:
 531              	.LBB80:
 532 01df 488D3D00 		leaq	.LC18(%rip), %rdi
 532      000000
 533 01e6 E8000000 		call	puts@PLT
 533      00
 534              	.LVL39:
 535              	.LBE80:
 536              	.LBE79:
 537              	.LBB81:
 538              	.LBB82:
 539 01eb 488D3D00 		leaq	.LC4(%rip), %rdi
 539      000000
 540 01f2 E8000000 		call	puts@PLT
 540      00
 541              	.LVL40:
 542              	.LBE82:
 543              	.LBE81:
 544              	.LBB83:
 545              	.LBB84:
 546 01f7 488D3D00 		leaq	.LC19(%rip), %rdi
 546      000000
 547 01fe E8000000 		call	puts@PLT
 547      00
 548              	.LVL41:
 549              	.LBE84:
 550              	.LBE83:
 551              	.LBB85:
 552              	.LBB86:
 553 0203 488D3D00 		leaq	.LC20(%rip), %rdi
 553      000000
 554 020a E8000000 		call	puts@PLT
 554      00
 555              	.LVL42:
 556              	.LBE86:
 557              	.LBE85:
 558              	.LBB87:
 559              	.LBB88:
 560 020f 488D3D00 		leaq	.LC21(%rip), %rdi
 560      000000
 561 0216 E8000000 		call	puts@PLT
 561      00
 562              	.LVL43:
 563              	.LBE88:
 564              	.LBE87:
 565              	.LBB89:
 566              	.LBB75:
 567 021b 488D3D00 		leaq	.LC4(%rip), %rdi
 567      000000
 568 0222 E8000000 		call	puts@PLT
 568      00
 569              	.LVL44:
 570 0227 660F280D 		movapd	.LC22(%rip), %xmm1
 570      00000000 
 571              	.LVL45:
 572 022f 90       		.p2align 4,,10
 573              		.p2align 3
 574              	.L18:
 575              	.LBE75:
 576              	.LBE89:
 141:stream_d.c    **** 	for (j=0; j<N; j++){
 577              		.loc 1 141 0
 578 0230 31C0     		xorl	%eax, %eax
 579 0232 0F294C24 		movaps	%xmm1, 16(%rsp)
 579      10
 580 0237 E8000000 		call	second@PLT
 580      00
 581              	.LVL46:
 582 023c 488D3500 		leaq	a(%rip), %rsi
 582      000000
 583 0243 488D3D00 		leaq	c(%rip), %rdi
 583      000000
 584 024a BA0008AF 		movl	$800000000, %edx
 584      2F
 585 024f F20F1144 		movsd	%xmm0, 8(%rsp)
 585      2408
 586 0255 E8000000 		call	memcpy@PLT
 586      00
 587              	.LVL47:
 145:stream_d.c    **** 	times[1][k] = second();
 588              		.loc 1 145 0
 589 025a 31C0     		xorl	%eax, %eax
 590 025c E8000000 		call	second@PLT
 590      00
 591              	.LVL48:
 592 0261 F20F5C44 		subsd	8(%rsp), %xmm0
 592      2408
 146:stream_d.c    **** 	for (j=0; j<N; j++)
 593              		.loc 1 146 0
 594 0267 31C0     		xorl	%eax, %eax
 145:stream_d.c    **** 	times[1][k] = second();
 595              		.loc 1 145 0
 596 0269 F20F1103 		movsd	%xmm0, (%rbx)
 146:stream_d.c    **** 	for (j=0; j<N; j++)
 597              		.loc 1 146 0
 598 026d E8000000 		call	second@PLT
 598      00
 599              	.LVL49:
 600 0272 660F284C 		movapd	16(%rsp), %xmm1
 600      2410
 601 0278 31C0     		xorl	%eax, %eax
 602 027a F20F1144 		movsd	%xmm0, 8(%rsp)
 602      2408
 603              	.LVL50:
 604              		.p2align 4,,10
 605              		.p2align 3
 606              	.L15:
 148:stream_d.c    **** 	times[1][k] = second() - times[1][k];
 607              		.loc 1 148 0 discriminator 3
 608 0280 66410F28 		movapd	(%r14,%rax), %xmm0
 608      0406
 609 0286 660F59C1 		mulpd	%xmm1, %xmm0
 610 028a 410F2944 		movaps	%xmm0, 0(%r13,%rax)
 610      0500
 611 0290 4883C010 		addq	$16, %rax
 612 0294 483D0008 		cmpq	$800000000, %rax
 612      AF2F
 613 029a 75E4     		jne	.L15
 149:stream_d.c    **** 	
 614              		.loc 1 149 0
 615 029c 31C0     		xorl	%eax, %eax
 616 029e 0F294C24 		movaps	%xmm1, 16(%rsp)
 616      10
 617 02a3 E8000000 		call	second@PLT
 617      00
 618              	.LVL51:
 151:stream_d.c    **** 	for (j=0; j<N; j++)
 619              		.loc 1 151 0
 620 02a8 31C0     		xorl	%eax, %eax
 149:stream_d.c    **** 	
 621              		.loc 1 149 0
 622 02aa F20F5C44 		subsd	8(%rsp), %xmm0
 622      2408
 623 02b0 F20F1143 		movsd	%xmm0, 80(%rbx)
 623      50
 151:stream_d.c    **** 	for (j=0; j<N; j++)
 624              		.loc 1 151 0
 625 02b5 E8000000 		call	second@PLT
 625      00
 626              	.LVL52:
 627 02ba 660F284C 		movapd	16(%rsp), %xmm1
 627      2410
 628 02c0 31C0     		xorl	%eax, %eax
 629 02c2 F20F1144 		movsd	%xmm0, 8(%rsp)
 629      2408
 630              	.LVL53:
 631 02c8 0F1F8400 		.p2align 4,,10
 631      00000000 
 632              		.p2align 3
 633              	.L16:
 153:stream_d.c    **** 	times[2][k] = second() - times[2][k];
 634              		.loc 1 153 0 discriminator 3
 635 02d0 66410F28 		movapd	(%r15,%rax), %xmm0
 635      0407
 636 02d6 66410F58 		addpd	0(%r13,%rax), %xmm0
 636      440500
 637 02dd 410F2904 		movaps	%xmm0, (%r14,%rax)
 637      06
 638 02e2 4883C010 		addq	$16, %rax
 639 02e6 483D0008 		cmpq	$800000000, %rax
 639      AF2F
 640 02ec 75E2     		jne	.L16
 154:stream_d.c    **** 	
 641              		.loc 1 154 0
 642 02ee 31C0     		xorl	%eax, %eax
 643 02f0 0F294C24 		movaps	%xmm1, 16(%rsp)
 643      10
 644 02f5 E8000000 		call	second@PLT
 644      00
 645              	.LVL54:
 156:stream_d.c    **** 	for (j=0; j<N; j++)
 646              		.loc 1 156 0
 647 02fa 31C0     		xorl	%eax, %eax
 154:stream_d.c    **** 	
 648              		.loc 1 154 0
 649 02fc F20F5C44 		subsd	8(%rsp), %xmm0
 649      2408
 650 0302 F20F1183 		movsd	%xmm0, 160(%rbx)
 650      A0000000 
 156:stream_d.c    **** 	for (j=0; j<N; j++)
 651              		.loc 1 156 0
 652 030a E8000000 		call	second@PLT
 652      00
 653              	.LVL55:
 654 030f 660F284C 		movapd	16(%rsp), %xmm1
 654      2410
 655 0315 31C0     		xorl	%eax, %eax
 656 0317 F20F1144 		movsd	%xmm0, 8(%rsp)
 656      2408
 657              	.LVL56:
 658 031d 0F1F00   		.p2align 4,,10
 659              		.p2align 3
 660              	.L17:
 158:stream_d.c    **** 	times[3][k] = second() - times[3][k];
 661              		.loc 1 158 0 discriminator 3
 662 0320 66410F28 		movapd	(%r14,%rax), %xmm0
 662      0406
 663 0326 660F59C1 		mulpd	%xmm1, %xmm0
 664 032a 66410F58 		addpd	0(%r13,%rax), %xmm0
 664      440500
 665 0331 410F2904 		movaps	%xmm0, (%r15,%rax)
 665      07
 666 0336 4883C010 		addq	$16, %rax
 667 033a 483D0008 		cmpq	$800000000, %rax
 667      AF2F
 668 0340 75DE     		jne	.L17
 159:stream_d.c    **** 	}
 669              		.loc 1 159 0 discriminator 2
 670 0342 31C0     		xorl	%eax, %eax
 671 0344 4883C308 		addq	$8, %rbx
 672 0348 0F294C24 		movaps	%xmm1, 16(%rsp)
 672      10
 673 034d E8000000 		call	second@PLT
 673      00
 674              	.LVL57:
 675 0352 F20F5C44 		subsd	8(%rsp), %xmm0
 675      2408
 139:stream_d.c    **** 	{
 676              		.loc 1 139 0 discriminator 2
 677 0358 660F284C 		movapd	16(%rsp), %xmm1
 677      2410
 159:stream_d.c    **** 	}
 678              		.loc 1 159 0 discriminator 2
 679 035e F20F1183 		movsd	%xmm0, 232(%rbx)
 679      E8000000 
 139:stream_d.c    **** 	{
 680              		.loc 1 139 0 discriminator 2
 681 0366 4C39E3   		cmpq	%r12, %rbx
 682 0369 0F85C1FE 		jne	.L18
 682      FFFF
 683 036f F20F1035 		movsd	rmstime(%rip), %xmm6
 683      00000000 
 684 0377 F20F1025 		movsd	mintime(%rip), %xmm4
 684      00000000 
 685 037f F20F1015 		movsd	maxtime(%rip), %xmm2
 685      00000000 
 686 0387 F2440F10 		movsd	8+rmstime(%rip), %xmm12
 686      25000000 
 686      00
 687 0390 F2440F10 		movsd	8+mintime(%rip), %xmm10
 687      15000000 
 687      00
 688 0399 F2440F10 		movsd	8+maxtime(%rip), %xmm8
 688      05000000 
 688      00
 689 03a2 F20F102D 		movsd	16+rmstime(%rip), %xmm5
 689      00000000 
 690 03aa F20F101D 		movsd	16+mintime(%rip), %xmm3
 690      00000000 
 691 03b2 F20F100D 		movsd	16+maxtime(%rip), %xmm1
 691      00000000 
 692 03ba F2440F10 		movsd	24+rmstime(%rip), %xmm11
 692      1D000000 
 692      00
 693 03c3 F2440F10 		movsd	24+mintime(%rip), %xmm9
 693      0D000000 
 693      00
 694 03cc F20F103D 		movsd	24+maxtime(%rip), %xmm7
 694      00000000 
 695              		.p2align 4,,10
 696 03d4 0F1F4000 		.p2align 3
 697              	.L35:
 698              	.LVL58:
 168:stream_d.c    **** 	    mintime[j] = MIN(mintime[j], times[j][k]);
 699              		.loc 1 168 0
 700 03d8 F20F1045 		movsd	0(%rbp), %xmm0
 700      00
 701 03dd 4883C508 		addq	$8, %rbp
 702 03e1 66440F28 		movapd	%xmm0, %xmm13
 702      E8
 703 03e6 F20F5DE0 		minsd	%xmm0, %xmm4
 704 03ea F2440F59 		mulsd	%xmm0, %xmm13
 704      E8
 705 03ef F20F5FD0 		maxsd	%xmm0, %xmm2
 706              	.LVL59:
 707 03f3 F20F1045 		movsd	72(%rbp), %xmm0
 707      48
 708 03f8 F2440F5D 		minsd	%xmm0, %xmm10
 708      D0
 709 03fd F2410F58 		addsd	%xmm13, %xmm6
 709      F5
 710 0402 66440F28 		movapd	%xmm0, %xmm13
 710      E8
 711 0407 F2440F5F 		maxsd	%xmm0, %xmm8
 711      C0
 712              	.LVL60:
 713 040c F2440F59 		mulsd	%xmm0, %xmm13
 713      E8
 714 0411 F20F1085 		movsd	152(%rbp), %xmm0
 714      98000000 
 715 0419 F20F5DD8 		minsd	%xmm0, %xmm3
 716 041d F2450F58 		addsd	%xmm13, %xmm12
 716      E5
 717 0422 66440F28 		movapd	%xmm0, %xmm13
 717      E8
 718 0427 F20F5FC8 		maxsd	%xmm0, %xmm1
 719              	.LVL61:
 720 042b F2440F59 		mulsd	%xmm0, %xmm13
 720      E8
 721 0430 F20F1085 		movsd	232(%rbp), %xmm0
 721      E8000000 
 164:stream_d.c    **** 	{
 722              		.loc 1 164 0
 723 0438 4839EB   		cmpq	%rbp, %rbx
 168:stream_d.c    **** 	    mintime[j] = MIN(mintime[j], times[j][k]);
 724              		.loc 1 168 0
 725 043b F2440F5D 		minsd	%xmm0, %xmm9
 725      C8
 726 0440 F2410F58 		addsd	%xmm13, %xmm5
 726      ED
 727 0445 66440F28 		movapd	%xmm0, %xmm13
 727      E8
 728 044a F20F5FF8 		maxsd	%xmm0, %xmm7
 729              	.LVL62:
 730 044e F2440F59 		mulsd	%xmm0, %xmm13
 730      E8
 731 0453 F2450F58 		addsd	%xmm13, %xmm11
 731      DD
 164:stream_d.c    **** 	{
 732              		.loc 1 164 0
 733 0458 0F857AFF 		jne	.L35
 733      FFFF
 734 045e 66410F14 		unpcklpd	%xmm12, %xmm6
 734      F4
 735              	.LBB90:
 736              	.LBB91:
 737              		.loc 2 104 0
 738 0463 488D3D00 		leaq	.LC23(%rip), %rdi
 738      000000
 739 046a 488D2D00 		leaq	rmstime(%rip), %rbp
 739      000000
 740 0471 66410F14 		unpcklpd	%xmm11, %xmm5
 740      EB
 741 0476 4C8D3D00 		leaq	mintime(%rip), %r15
 741      000000
 742 047d 66410F14 		unpcklpd	%xmm10, %xmm4
 742      E2
 743 0482 4C8D3500 		leaq	maxtime(%rip), %r14
 743      000000
 744 0489 66410F14 		unpcklpd	%xmm9, %xmm3
 744      D9
 745 048e 4C8D2D00 		leaq	bytes(%rip), %r13
 745      000000
 746 0495 66410F14 		unpcklpd	%xmm8, %xmm2
 746      D0
 747 049a 4C8D2500 		leaq	label(%rip), %r12
 747      000000
 748 04a1 660F14CF 		unpcklpd	%xmm7, %xmm1
 749 04a5 31DB     		xorl	%ebx, %ebx
 750 04a7 0F293500 		movaps	%xmm6, rmstime(%rip)
 750      000000
 751 04ae 0F292D00 		movaps	%xmm5, 16+rmstime(%rip)
 751      000000
 752 04b5 0F292500 		movaps	%xmm4, mintime(%rip)
 752      000000
 753 04bc 0F291D00 		movaps	%xmm3, 16+mintime(%rip)
 753      000000
 754 04c3 0F291500 		movaps	%xmm2, maxtime(%rip)
 754      000000
 755 04ca 0F290D00 		movaps	%xmm1, 16+maxtime(%rip)
 755      000000
 756              	.LVL63:
 757 04d1 E8000000 		call	puts@PLT
 757      00
 758              	.LVL64:
 759              	.L36:
 760 04d6 660FEFDB 		pxor	%xmm3, %xmm3
 761              	.LBE91:
 762              	.LBE90:
 176:stream_d.c    **** 
 763              		.loc 1 176 0 discriminator 3
 764 04da F20F1044 		movsd	0(%rbp,%rbx), %xmm0
 764      1D00
 765 04e0 F20F5E05 		divsd	.LC24(%rip), %xmm0
 765      00000000 
 766 04e8 660F2ED8 		ucomisd	%xmm0, %xmm3
 767 04ec F20F51C8 		sqrtsd	%xmm0, %xmm1
 768 04f0 0F878600 		ja	.L57
 768      0000
 769              	.L37:
 179:stream_d.c    **** 	       rmstime[j],
 770              		.loc 1 179 0 discriminator 3
 771 04f6 F20F1005 		movsd	.LC0(%rip), %xmm0
 771      00000000 
 772              	.LBB92:
 773              	.LBB93:
 774              		.loc 2 104 0 discriminator 3
 775 04fe 498B141C 		movq	(%r12,%rbx), %rdx
 776 0502 488D3500 		leaq	.LC26(%rip), %rsi
 776      000000
 777              	.LBE93:
 778              	.LBE92:
 178:stream_d.c    **** 	       1.0E-06 * bytes[j]/mintime[j],
 779              		.loc 1 178 0 discriminator 3
 780 0509 F2410F10 		movsd	(%r15,%rbx), %xmm2
 780      141F
 781              	.LVL65:
 782              	.LBB97:
 783              	.LBB94:
 784              		.loc 2 104 0 discriminator 3
 785 050f BF010000 		movl	$1, %edi
 785      00
 786              	.LBE94:
 787              	.LBE97:
 179:stream_d.c    **** 	       rmstime[j],
 788              		.loc 1 179 0 discriminator 3
 789 0514 F2410F59 		mulsd	0(%r13,%rbx), %xmm0
 789      441D00
 176:stream_d.c    **** 
 790              		.loc 1 176 0 discriminator 3
 791 051b F20F114C 		movsd	%xmm1, 0(%rbp,%rbx)
 791      1D00
 792              	.LBB98:
 793              	.LBB95:
 794              		.loc 2 104 0 discriminator 3
 795 0521 B8040000 		movl	$4, %eax
 795      00
 796 0526 F2410F10 		movsd	(%r14,%rbx), %xmm3
 796      1C1E
 797 052c 4883C308 		addq	$8, %rbx
 798              	.LBE95:
 799              	.LBE98:
 178:stream_d.c    **** 	       1.0E-06 * bytes[j]/mintime[j],
 800              		.loc 1 178 0 discriminator 3
 801 0530 F20F5EC2 		divsd	%xmm2, %xmm0
 802              	.LBB99:
 803              	.LBB96:
 804              		.loc 2 104 0 discriminator 3
 805 0534 E8000000 		call	__printf_chk@PLT
 805      00
 806              	.LVL66:
 807              	.LBE96:
 808              	.LBE99:
 175:stream_d.c    **** 	rmstime[j] = sqrt(rmstime[j]/(double)NTIMES);
 809              		.loc 1 175 0 discriminator 3
 810 0539 4883FB20 		cmpq	$32, %rbx
 811 053d 7597     		jne	.L36
 185:stream_d.c    **** 
 812              		.loc 1 185 0
 813 053f 31C0     		xorl	%eax, %eax
 814 0541 488B8C24 		movq	360(%rsp), %rcx
 814      68010000 
 815 0549 6448330C 		xorq	%fs:40, %rcx
 815      25280000 
 815      00
 816 0552 7523     		jne	.L58
 817 0554 4881C478 		addq	$376, %rsp
 817      010000
 818              		.cfi_remember_state
 819              		.cfi_def_cfa_offset 56
 820 055b 5B       		popq	%rbx
 821              		.cfi_def_cfa_offset 48
 822 055c 5D       		popq	%rbp
 823              		.cfi_def_cfa_offset 40
 824 055d 415C     		popq	%r12
 825              		.cfi_def_cfa_offset 32
 826 055f 415D     		popq	%r13
 827              		.cfi_def_cfa_offset 24
 828 0561 415E     		popq	%r14
 829              		.cfi_def_cfa_offset 16
 830 0563 415F     		popq	%r15
 831              		.cfi_def_cfa_offset 8
 832 0565 C3       		ret
 833              	.LVL67:
 834              	.L12:
 835              		.cfi_restore_state
 836              	.LBB100:
 837              	.LBB101:
 838              		.loc 2 104 0
 839 0566 488D3D00 		leaq	.LC14(%rip), %rdi
 839      000000
 840 056d E8000000 		call	puts@PLT
 840      00
 841              	.LVL68:
 842 0572 E9B5FBFF 		jmp	.L13
 842      FF
 843              	.LVL69:
 844              	.L58:
 845              	.LBE101:
 846              	.LBE100:
 185:stream_d.c    **** 
 847              		.loc 1 185 0
 848 0577 E8000000 		call	__stack_chk_fail@PLT
 848      00
 849              	.LVL70:
 850              	.L57:
 851 057c F20F114C 		movsd	%xmm1, 8(%rsp)
 851      2408
 176:stream_d.c    **** 
 852              		.loc 1 176 0 discriminator 3
 853 0582 E8000000 		call	sqrt@PLT
 853      00
 854              	.LVL71:
 855 0587 F20F104C 		movsd	8(%rsp), %xmm1
 855      2408
 856 058d E964FFFF 		jmp	.L37
 856      FF
 857              		.cfi_endproc
 858              	.LFE23:
 860              		.section	.rodata
 861              		.align 32
 864              	bytes:
 865 0000 00000000 		.long	0
 866 0004 84D7D741 		.long	1104664452
 867 0008 00000000 		.long	0
 868 000c 84D7D741 		.long	1104664452
 869 0010 00000000 		.long	0
 870 0014 A3E1E141 		.long	1105322403
 871 0018 00000000 		.long	0
 872 001c A3E1E141 		.long	1105322403
 873              		.section	.rodata.str1.1
 874              	.LC27:
 875 0035 436F7079 		.string	"Copy:      "
 875      3A202020 
 875      20202000 
 876              	.LC28:
 877 0041 5363616C 		.string	"Scale:     "
 877      653A2020 
 877      20202000 
 878              	.LC29:
 879 004d 4164643A 		.string	"Add:       "
 879      20202020 
 879      20202000 
 880              	.LC30:
 881 0059 54726961 		.string	"Triad:     "
 881      643A2020 
 881      20202000 
 882              		.section	.data.rel.ro.local,"aw",@progbits
 883              		.align 32
 886              	label:
 887 0000 00000000 		.quad	.LC27
 887      00000000 
 888 0008 00000000 		.quad	.LC28
 888      00000000 
 889 0010 00000000 		.quad	.LC29
 889      00000000 
 890 0018 00000000 		.quad	.LC30
 890      00000000 
 891              		.data
 892              		.align 32
 895              	mintime:
 896 0000 000000E0 		.long	3758096384
 897 0004 FFFFEF47 		.long	1206910975
 898 0008 000000E0 		.long	3758096384
 899 000c FFFFEF47 		.long	1206910975
 900 0010 000000E0 		.long	3758096384
 901 0014 FFFFEF47 		.long	1206910975
 902 0018 000000E0 		.long	3758096384
 903 001c FFFFEF47 		.long	1206910975
 904              		.local	maxtime
 905              		.comm	maxtime,32,32
 906              		.local	rmstime
 907              		.comm	rmstime,32,32
 908              		.local	c
 909              		.comm	c,800000000,32
 910              		.local	b
 911              		.comm	b,800000000,32
 912              		.local	a
 913              		.comm	a,800000000,32
 914              		.section	.rodata.cst8,"aM",@progbits,8
 915              		.align 8
 916              	.LC0:
 917 0000 8DEDB5A0 		.long	2696277389
 918 0004 F7C6B03E 		.long	1051772663
 919              		.section	.rodata.cst16,"aM",@progbits,16
 920              		.align 16
 921              	.LC1:
 922 0000 00000000 		.long	0
 923 0004 80842E41 		.long	1093567616
 924 0008 00000000 		.long	0
 925 000c 80842E41 		.long	1093567616
 926              		.align 16
 927              	.LC2:
 928 0010 40420F00 		.long	1000000
 929 0014 40420F00 		.long	1000000
 930 0018 40420F00 		.long	1000000
 931 001c 40420F00 		.long	1000000
 932              		.section	.rodata.cst8
 933              		.align 8
 934              	.LC3:
 935 0008 00000000 		.long	0
 936 000c 80842E41 		.long	1093567616
 937              		.align 8
 938              	.LC7:
 939 0010 00000000 		.long	0
 940 0014 BA3C9CC0 		.long	-1063502662
 941              		.section	.rodata.cst16
 942              		.align 16
 943              	.LC11:
 944 0020 00000000 		.long	0
 945 0024 0000F03F 		.long	1072693248
 946 0028 00000000 		.long	0
 947 002c 0000F03F 		.long	1072693248
 948              		.align 16
 949              	.LC12:
 950 0030 00000000 		.long	0
 951 0034 00000040 		.long	1073741824
 952 0038 00000000 		.long	0
 953 003c 00000040 		.long	1073741824
 954              		.align 16
 955              	.LC22:
 956 0040 00000000 		.long	0
 957 0044 00000840 		.long	1074266112
 958 0048 00000000 		.long	0
 959 004c 00000840 		.long	1074266112
 960              		.section	.rodata.cst8
 961              		.align 8
 962              	.LC24:
 963 0018 00000000 		.long	0
 964 001c 00002440 		.long	1076101120
 965              		.text
 966              	.Letext0:
 967              		.file 3 "/usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h"
 968              		.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
 969              		.file 5 "/usr/include/x86_64-linux-gnu/bits/libio.h"
 970              		.file 6 "/usr/include/stdio.h"
 971              		.file 7 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
 972              		.file 8 "/usr/include/math.h"
 973              		.file 9 "/usr/include/x86_64-linux-gnu/sys/time.h"
 974              		.file 10 "<built-in>"
 975              		.file 11 "/usr/include/x86_64-linux-gnu/bits/mathcalls.h"
