   1              		.file	"matmult.c"
   2              		.text
   3              	.Ltext0:
   4              		.globl	fullmatvectmult
   6              	fullmatvectmult:
   7              	.LFB5:
   8              		.file 1 "src/matrix/matmult.c"
   1:src/matrix/matmult.c **** #include "genmat.h"
   2:src/matrix/matmult.c **** #include <time.h>
   3:src/matrix/matmult.c **** #include <stdlib.h>
   4:src/matrix/matmult.c **** 
   5:src/matrix/matmult.c **** void fullmatvectmult(double *fullmat, double *vect, int size, double *res)
   6:src/matrix/matmult.c **** {
   9              		.loc 1 6 0
  10              		.cfi_startproc
  11 0000 55       		pushq	%rbp
  12              		.cfi_def_cfa_offset 16
  13              		.cfi_offset 6, -16
  14 0001 4889E5   		movq	%rsp, %rbp
  15              		.cfi_def_cfa_register 6
  16 0004 4883EC30 		subq	$48, %rsp
  17 0008 FF150000 	1:	call	*mcount@GOTPCREL(%rip)
  17      0000
  18 000e 48897DE8 		movq	%rdi, -24(%rbp)
  19 0012 488975E0 		movq	%rsi, -32(%rbp)
  20 0016 8955DC   		movl	%edx, -36(%rbp)
  21 0019 48894DD0 		movq	%rcx, -48(%rbp)
   7:src/matrix/matmult.c ****   int i, j;
   8:src/matrix/matmult.c **** 
   9:src/matrix/matmult.c ****   for (i = 0; i < size; ++i)
  22              		.loc 1 9 0
  23 001d C745F800 		movl	$0, -8(%rbp)
  23      000000
  24 0024 E98C0000 		jmp	.L2
  24      00
  25              	.L5:
  10:src/matrix/matmult.c ****   {
  11:src/matrix/matmult.c ****     for (j = 0; j < size; ++j)
  26              		.loc 1 11 0
  27 0029 C745FC00 		movl	$0, -4(%rbp)
  27      000000
  28 0030 EB77     		jmp	.L3
  29              	.L4:
  12:src/matrix/matmult.c ****     {
  13:src/matrix/matmult.c ****       res[i] += fullmat[i * size + j] * vect[j];
  30              		.loc 1 13 0 discriminator 3
  31 0032 8B45F8   		movl	-8(%rbp), %eax
  32 0035 4898     		cltq
  33 0037 488D14C5 		leaq	0(,%rax,8), %rdx
  33      00000000 
  34 003f 488B45D0 		movq	-48(%rbp), %rax
  35 0043 4801D0   		addq	%rdx, %rax
  36 0046 F20F1008 		movsd	(%rax), %xmm1
  37 004a 8B45F8   		movl	-8(%rbp), %eax
  38 004d 0FAF45DC 		imull	-36(%rbp), %eax
  39 0051 89C2     		movl	%eax, %edx
  40 0053 8B45FC   		movl	-4(%rbp), %eax
  41 0056 01D0     		addl	%edx, %eax
  42 0058 4898     		cltq
  43 005a 488D14C5 		leaq	0(,%rax,8), %rdx
  43      00000000 
  44 0062 488B45E8 		movq	-24(%rbp), %rax
  45 0066 4801D0   		addq	%rdx, %rax
  46 0069 F20F1010 		movsd	(%rax), %xmm2
  47 006d 8B45FC   		movl	-4(%rbp), %eax
  48 0070 4898     		cltq
  49 0072 488D14C5 		leaq	0(,%rax,8), %rdx
  49      00000000 
  50 007a 488B45E0 		movq	-32(%rbp), %rax
  51 007e 4801D0   		addq	%rdx, %rax
  52 0081 F20F1000 		movsd	(%rax), %xmm0
  53 0085 F20F59C2 		mulsd	%xmm2, %xmm0
  54 0089 8B45F8   		movl	-8(%rbp), %eax
  55 008c 4898     		cltq
  56 008e 488D14C5 		leaq	0(,%rax,8), %rdx
  56      00000000 
  57 0096 488B45D0 		movq	-48(%rbp), %rax
  58 009a 4801D0   		addq	%rdx, %rax
  59 009d F20F58C1 		addsd	%xmm1, %xmm0
  60 00a1 F20F1100 		movsd	%xmm0, (%rax)
  11:src/matrix/matmult.c ****     {
  61              		.loc 1 11 0 discriminator 3
  62 00a5 8345FC01 		addl	$1, -4(%rbp)
  63              	.L3:
  11:src/matrix/matmult.c ****     {
  64              		.loc 1 11 0 is_stmt 0 discriminator 1
  65 00a9 8B45FC   		movl	-4(%rbp), %eax
  66 00ac 3B45DC   		cmpl	-36(%rbp), %eax
  67 00af 7C81     		jl	.L4
   9:src/matrix/matmult.c ****   {
  68              		.loc 1 9 0 is_stmt 1 discriminator 2
  69 00b1 8345F801 		addl	$1, -8(%rbp)
  70              	.L2:
   9:src/matrix/matmult.c ****   {
  71              		.loc 1 9 0 is_stmt 0 discriminator 1
  72 00b5 8B45F8   		movl	-8(%rbp), %eax
  73 00b8 3B45DC   		cmpl	-36(%rbp), %eax
  74 00bb 0F8C68FF 		jl	.L5
  74      FFFF
  14:src/matrix/matmult.c ****     }
  15:src/matrix/matmult.c ****   }
  16:src/matrix/matmult.c **** }
  75              		.loc 1 16 0 is_stmt 1
  76 00c1 90       		nop
  77 00c2 C9       		leave
  78              		.cfi_def_cfa 7, 8
  79 00c3 C3       		ret
  80              		.cfi_endproc
  81              	.LFE5:
  83              		.globl	csmatvectmul
  85              	csmatvectmul:
  86              	.LFB6:
  17:src/matrix/matmult.c **** 
  18:src/matrix/matmult.c **** void csmatvectmul(CSrc *cs, double *vect, double *res)
  19:src/matrix/matmult.c **** {
  87              		.loc 1 19 0
  88              		.cfi_startproc
  89 00c4 55       		pushq	%rbp
  90              		.cfi_def_cfa_offset 16
  91              		.cfi_offset 6, -16
  92 00c5 4889E5   		movq	%rsp, %rbp
  93              		.cfi_def_cfa_register 6
  94 00c8 4883EC48 		subq	$72, %rsp
  95 00cc FF150000 	1:	call	*mcount@GOTPCREL(%rip)
  95      0000
  96 00d2 48897DC8 		movq	%rdi, -56(%rbp)
  97 00d6 488975C0 		movq	%rsi, -64(%rbp)
  98 00da 488955B8 		movq	%rdx, -72(%rbp)
  20:src/matrix/matmult.c ****   int i, j;
  21:src/matrix/matmult.c **** 
  22:src/matrix/matmult.c ****   int *ptr = cs->ptr;
  99              		.loc 1 22 0
 100 00de 488B45C8 		movq	-56(%rbp), %rax
 101 00e2 488B4010 		movq	16(%rax), %rax
 102 00e6 488945E8 		movq	%rax, -24(%rbp)
  23:src/matrix/matmult.c ****   int *ind = cs->ind;
 103              		.loc 1 23 0
 104 00ea 488B45C8 		movq	-56(%rbp), %rax
 105 00ee 488B4008 		movq	8(%rax), %rax
 106 00f2 488945F0 		movq	%rax, -16(%rbp)
  24:src/matrix/matmult.c ****   double *val = cs->val;
 107              		.loc 1 24 0
 108 00f6 488B45C8 		movq	-56(%rbp), %rax
 109 00fa 488B00   		movq	(%rax), %rax
 110 00fd 488945F8 		movq	%rax, -8(%rbp)
  25:src/matrix/matmult.c ****   int n = cs->SData->n;
 111              		.loc 1 25 0
 112 0101 488B45C8 		movq	-56(%rbp), %rax
 113 0105 488B4018 		movq	24(%rax), %rax
 114 0109 8B00     		movl	(%rax), %eax
 115 010b 8945E4   		movl	%eax, -28(%rbp)
  26:src/matrix/matmult.c **** 
  27:src/matrix/matmult.c ****   for (i = 0; i < n; ++i)
 116              		.loc 1 27 0
 117 010e C745DC00 		movl	$0, -36(%rbp)
 117      000000
 118 0115 E9C10000 		jmp	.L7
 118      00
 119              	.L10:
  28:src/matrix/matmult.c ****   {
  29:src/matrix/matmult.c ****     for (j = ptr[i]; j < ptr[i + 1]; ++j)
 120              		.loc 1 29 0
 121 011a 8B45DC   		movl	-36(%rbp), %eax
 122 011d 4898     		cltq
 123 011f 488D1485 		leaq	0(,%rax,4), %rdx
 123      00000000 
 124 0127 488B45E8 		movq	-24(%rbp), %rax
 125 012b 4801D0   		addq	%rdx, %rax
 126 012e 8B00     		movl	(%rax), %eax
 127 0130 8945E0   		movl	%eax, -32(%rbp)
 128 0133 EB7F     		jmp	.L8
 129              	.L9:
  30:src/matrix/matmult.c ****     {
  31:src/matrix/matmult.c ****       res[i] += vect[ind[j]] * val[j];
 130              		.loc 1 31 0 discriminator 3
 131 0135 8B45DC   		movl	-36(%rbp), %eax
 132 0138 4898     		cltq
 133 013a 488D14C5 		leaq	0(,%rax,8), %rdx
 133      00000000 
 134 0142 488B45B8 		movq	-72(%rbp), %rax
 135 0146 4801D0   		addq	%rdx, %rax
 136 0149 F20F1008 		movsd	(%rax), %xmm1
 137 014d 8B45E0   		movl	-32(%rbp), %eax
 138 0150 4898     		cltq
 139 0152 488D1485 		leaq	0(,%rax,4), %rdx
 139      00000000 
 140 015a 488B45F0 		movq	-16(%rbp), %rax
 141 015e 4801D0   		addq	%rdx, %rax
 142 0161 8B00     		movl	(%rax), %eax
 143 0163 4898     		cltq
 144 0165 488D14C5 		leaq	0(,%rax,8), %rdx
 144      00000000 
 145 016d 488B45C0 		movq	-64(%rbp), %rax
 146 0171 4801D0   		addq	%rdx, %rax
 147 0174 F20F1010 		movsd	(%rax), %xmm2
 148 0178 8B45E0   		movl	-32(%rbp), %eax
 149 017b 4898     		cltq
 150 017d 488D14C5 		leaq	0(,%rax,8), %rdx
 150      00000000 
 151 0185 488B45F8 		movq	-8(%rbp), %rax
 152 0189 4801D0   		addq	%rdx, %rax
 153 018c F20F1000 		movsd	(%rax), %xmm0
 154 0190 F20F59C2 		mulsd	%xmm2, %xmm0
 155 0194 8B45DC   		movl	-36(%rbp), %eax
 156 0197 4898     		cltq
 157 0199 488D14C5 		leaq	0(,%rax,8), %rdx
 157      00000000 
 158 01a1 488B45B8 		movq	-72(%rbp), %rax
 159 01a5 4801D0   		addq	%rdx, %rax
 160 01a8 F20F58C1 		addsd	%xmm1, %xmm0
 161 01ac F20F1100 		movsd	%xmm0, (%rax)
  29:src/matrix/matmult.c ****     {
 162              		.loc 1 29 0 discriminator 3
 163 01b0 8345E001 		addl	$1, -32(%rbp)
 164              	.L8:
  29:src/matrix/matmult.c ****     {
 165              		.loc 1 29 0 is_stmt 0 discriminator 1
 166 01b4 8B45DC   		movl	-36(%rbp), %eax
 167 01b7 4898     		cltq
 168 01b9 4883C001 		addq	$1, %rax
 169 01bd 488D1485 		leaq	0(,%rax,4), %rdx
 169      00000000 
 170 01c5 488B45E8 		movq	-24(%rbp), %rax
 171 01c9 4801D0   		addq	%rdx, %rax
 172 01cc 8B00     		movl	(%rax), %eax
 173 01ce 3945E0   		cmpl	%eax, -32(%rbp)
 174 01d1 0F8C5EFF 		jl	.L9
 174      FFFF
  27:src/matrix/matmult.c ****   {
 175              		.loc 1 27 0 is_stmt 1 discriminator 2
 176 01d7 8345DC01 		addl	$1, -36(%rbp)
 177              	.L7:
  27:src/matrix/matmult.c ****   {
 178              		.loc 1 27 0 is_stmt 0 discriminator 1
 179 01db 8B45DC   		movl	-36(%rbp), %eax
 180 01de 3B45E4   		cmpl	-28(%rbp), %eax
 181 01e1 0F8C33FF 		jl	.L10
 181      FFFF
  32:src/matrix/matmult.c ****     }
  33:src/matrix/matmult.c ****   }
  34:src/matrix/matmult.c **** }
 182              		.loc 1 34 0 is_stmt 1
 183 01e7 90       		nop
 184 01e8 C9       		leave
 185              		.cfi_def_cfa 7, 8
 186 01e9 C3       		ret
 187              		.cfi_endproc
 188              	.LFE6:
 190              		.globl	csmatvectmul_opt
 192              	csmatvectmul_opt:
 193              	.LFB7:
  35:src/matrix/matmult.c **** 
  36:src/matrix/matmult.c **** void csmatvectmul_opt(CSrc *cs, double *vect, double *res)
  37:src/matrix/matmult.c **** {
 194              		.loc 1 37 0
 195              		.cfi_startproc
 196 01ea 55       		pushq	%rbp
 197              		.cfi_def_cfa_offset 16
 198              		.cfi_offset 6, -16
 199 01eb 4889E5   		movq	%rsp, %rbp
 200              		.cfi_def_cfa_register 6
 201 01ee 4883EC58 		subq	$88, %rsp
 202 01f2 FF150000 	1:	call	*mcount@GOTPCREL(%rip)
 202      0000
 203 01f8 48897DB8 		movq	%rdi, -72(%rbp)
 204 01fc 488975B0 		movq	%rsi, -80(%rbp)
 205 0200 488955A8 		movq	%rdx, -88(%rbp)
  38:src/matrix/matmult.c ****   int i, j;
  39:src/matrix/matmult.c **** 
  40:src/matrix/matmult.c ****   int *ptr = cs->ptr;
 206              		.loc 1 40 0
 207 0204 488B45B8 		movq	-72(%rbp), %rax
 208 0208 488B4010 		movq	16(%rax), %rax
 209 020c 488945E8 		movq	%rax, -24(%rbp)
  41:src/matrix/matmult.c ****   int *ind = cs->ind;
 210              		.loc 1 41 0
 211 0210 488B45B8 		movq	-72(%rbp), %rax
 212 0214 488B4008 		movq	8(%rax), %rax
 213 0218 488945F0 		movq	%rax, -16(%rbp)
  42:src/matrix/matmult.c ****   double *val = cs->val;
 214              		.loc 1 42 0
 215 021c 488B45B8 		movq	-72(%rbp), %rax
 216 0220 488B00   		movq	(%rax), %rax
 217 0223 488945F8 		movq	%rax, -8(%rbp)
  43:src/matrix/matmult.c ****   int n = cs->SData->n;
 218              		.loc 1 43 0
 219 0227 488B45B8 		movq	-72(%rbp), %rax
 220 022b 488B4018 		movq	24(%rax), %rax
 221 022f 8B00     		movl	(%rax), %eax
 222 0231 8945D4   		movl	%eax, -44(%rbp)
  44:src/matrix/matmult.c **** 
  45:src/matrix/matmult.c ****   for (i = 0; i < n; ++i)
 223              		.loc 1 45 0
 224 0234 C745CC00 		movl	$0, -52(%rbp)
 224      000000
 225 023b E9C40200 		jmp	.L12
 225      00
 226              	.L21:
 227              	.LBB2:
  46:src/matrix/matmult.c ****   {
  47:src/matrix/matmult.c ****     double temp = 0;
 228              		.loc 1 47 0
 229 0240 660FEFC0 		pxor	%xmm0, %xmm0
 230 0244 F20F1145 		movsd	%xmm0, -32(%rbp)
 230      E0
  48:src/matrix/matmult.c ****     int from = ptr[i], to = ptr[i + 1];
 231              		.loc 1 48 0
 232 0249 8B45CC   		movl	-52(%rbp), %eax
 233 024c 4898     		cltq
 234 024e 488D1485 		leaq	0(,%rax,4), %rdx
 234      00000000 
 235 0256 488B45E8 		movq	-24(%rbp), %rax
 236 025a 4801D0   		addq	%rdx, %rax
 237 025d 8B00     		movl	(%rax), %eax
 238 025f 8945D8   		movl	%eax, -40(%rbp)
 239 0262 8B45CC   		movl	-52(%rbp), %eax
 240 0265 4898     		cltq
 241 0267 4883C001 		addq	$1, %rax
 242 026b 488D1485 		leaq	0(,%rax,4), %rdx
 242      00000000 
 243 0273 488B45E8 		movq	-24(%rbp), %rax
 244 0277 4801D0   		addq	%rdx, %rax
 245 027a 8B00     		movl	(%rax), %eax
 246 027c 8945DC   		movl	%eax, -36(%rbp)
  49:src/matrix/matmult.c **** 
  50:src/matrix/matmult.c ****     if (to - from > 3)
 247              		.loc 1 50 0
 248 027f 8B45DC   		movl	-36(%rbp), %eax
 249 0282 2B45D8   		subl	-40(%rbp), %eax
 250 0285 83F803   		cmpl	$3, %eax
 251 0288 0F8EEC01 		jle	.L13
 251      0000
  51:src/matrix/matmult.c ****     {
  52:src/matrix/matmult.c ****       for (j = from; j < to; j += 4)
 252              		.loc 1 52 0
 253 028e 8B45D8   		movl	-40(%rbp), %eax
 254 0291 8945D0   		movl	%eax, -48(%rbp)
 255 0294 E9700100 		jmp	.L14
 255      00
 256              	.L15:
  53:src/matrix/matmult.c ****       {
  54:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
 257              		.loc 1 54 0 discriminator 3
 258 0299 8B45D0   		movl	-48(%rbp), %eax
 259 029c 4898     		cltq
 260 029e 488D1485 		leaq	0(,%rax,4), %rdx
 260      00000000 
 261 02a6 488B45F0 		movq	-16(%rbp), %rax
 262 02aa 4801D0   		addq	%rdx, %rax
 263 02ad 8B00     		movl	(%rax), %eax
 264 02af 4898     		cltq
 265 02b1 488D14C5 		leaq	0(,%rax,8), %rdx
 265      00000000 
 266 02b9 488B45B0 		movq	-80(%rbp), %rax
 267 02bd 4801D0   		addq	%rdx, %rax
 268 02c0 F20F1008 		movsd	(%rax), %xmm1
 269 02c4 8B45D0   		movl	-48(%rbp), %eax
 270 02c7 4898     		cltq
 271 02c9 488D14C5 		leaq	0(,%rax,8), %rdx
 271      00000000 
 272 02d1 488B45F8 		movq	-8(%rbp), %rax
 273 02d5 4801D0   		addq	%rdx, %rax
 274 02d8 F20F1000 		movsd	(%rax), %xmm0
 275 02dc F20F59C1 		mulsd	%xmm1, %xmm0
 276 02e0 F20F104D 		movsd	-32(%rbp), %xmm1
 276      E0
 277 02e5 F20F58C1 		addsd	%xmm1, %xmm0
 278 02e9 F20F1145 		movsd	%xmm0, -32(%rbp)
 278      E0
  55:src/matrix/matmult.c ****         temp += vect[ind[j+1]] * val[j+1];
 279              		.loc 1 55 0 discriminator 3
 280 02ee 8B45D0   		movl	-48(%rbp), %eax
 281 02f1 4898     		cltq
 282 02f3 4883C001 		addq	$1, %rax
 283 02f7 488D1485 		leaq	0(,%rax,4), %rdx
 283      00000000 
 284 02ff 488B45F0 		movq	-16(%rbp), %rax
 285 0303 4801D0   		addq	%rdx, %rax
 286 0306 8B00     		movl	(%rax), %eax
 287 0308 4898     		cltq
 288 030a 488D14C5 		leaq	0(,%rax,8), %rdx
 288      00000000 
 289 0312 488B45B0 		movq	-80(%rbp), %rax
 290 0316 4801D0   		addq	%rdx, %rax
 291 0319 F20F1008 		movsd	(%rax), %xmm1
 292 031d 8B45D0   		movl	-48(%rbp), %eax
 293 0320 4898     		cltq
 294 0322 4883C001 		addq	$1, %rax
 295 0326 488D14C5 		leaq	0(,%rax,8), %rdx
 295      00000000 
 296 032e 488B45F8 		movq	-8(%rbp), %rax
 297 0332 4801D0   		addq	%rdx, %rax
 298 0335 F20F1000 		movsd	(%rax), %xmm0
 299 0339 F20F59C1 		mulsd	%xmm1, %xmm0
 300 033d F20F104D 		movsd	-32(%rbp), %xmm1
 300      E0
 301 0342 F20F58C1 		addsd	%xmm1, %xmm0
 302 0346 F20F1145 		movsd	%xmm0, -32(%rbp)
 302      E0
  56:src/matrix/matmult.c ****         temp += vect[ind[j+2]] * val[j+2];
 303              		.loc 1 56 0 discriminator 3
 304 034b 8B45D0   		movl	-48(%rbp), %eax
 305 034e 4898     		cltq
 306 0350 4883C002 		addq	$2, %rax
 307 0354 488D1485 		leaq	0(,%rax,4), %rdx
 307      00000000 
 308 035c 488B45F0 		movq	-16(%rbp), %rax
 309 0360 4801D0   		addq	%rdx, %rax
 310 0363 8B00     		movl	(%rax), %eax
 311 0365 4898     		cltq
 312 0367 488D14C5 		leaq	0(,%rax,8), %rdx
 312      00000000 
 313 036f 488B45B0 		movq	-80(%rbp), %rax
 314 0373 4801D0   		addq	%rdx, %rax
 315 0376 F20F1008 		movsd	(%rax), %xmm1
 316 037a 8B45D0   		movl	-48(%rbp), %eax
 317 037d 4898     		cltq
 318 037f 4883C002 		addq	$2, %rax
 319 0383 488D14C5 		leaq	0(,%rax,8), %rdx
 319      00000000 
 320 038b 488B45F8 		movq	-8(%rbp), %rax
 321 038f 4801D0   		addq	%rdx, %rax
 322 0392 F20F1000 		movsd	(%rax), %xmm0
 323 0396 F20F59C1 		mulsd	%xmm1, %xmm0
 324 039a F20F104D 		movsd	-32(%rbp), %xmm1
 324      E0
 325 039f F20F58C1 		addsd	%xmm1, %xmm0
 326 03a3 F20F1145 		movsd	%xmm0, -32(%rbp)
 326      E0
  57:src/matrix/matmult.c ****         temp += vect[ind[j+3]] * val[j+3];
 327              		.loc 1 57 0 discriminator 3
 328 03a8 8B45D0   		movl	-48(%rbp), %eax
 329 03ab 4898     		cltq
 330 03ad 4883C003 		addq	$3, %rax
 331 03b1 488D1485 		leaq	0(,%rax,4), %rdx
 331      00000000 
 332 03b9 488B45F0 		movq	-16(%rbp), %rax
 333 03bd 4801D0   		addq	%rdx, %rax
 334 03c0 8B00     		movl	(%rax), %eax
 335 03c2 4898     		cltq
 336 03c4 488D14C5 		leaq	0(,%rax,8), %rdx
 336      00000000 
 337 03cc 488B45B0 		movq	-80(%rbp), %rax
 338 03d0 4801D0   		addq	%rdx, %rax
 339 03d3 F20F1008 		movsd	(%rax), %xmm1
 340 03d7 8B45D0   		movl	-48(%rbp), %eax
 341 03da 4898     		cltq
 342 03dc 4883C003 		addq	$3, %rax
 343 03e0 488D14C5 		leaq	0(,%rax,8), %rdx
 343      00000000 
 344 03e8 488B45F8 		movq	-8(%rbp), %rax
 345 03ec 4801D0   		addq	%rdx, %rax
 346 03ef F20F1000 		movsd	(%rax), %xmm0
 347 03f3 F20F59C1 		mulsd	%xmm1, %xmm0
 348 03f7 F20F104D 		movsd	-32(%rbp), %xmm1
 348      E0
 349 03fc F20F58C1 		addsd	%xmm1, %xmm0
 350 0400 F20F1145 		movsd	%xmm0, -32(%rbp)
 350      E0
  52:src/matrix/matmult.c ****       {
 351              		.loc 1 52 0 discriminator 3
 352 0405 8345D004 		addl	$4, -48(%rbp)
 353              	.L14:
  52:src/matrix/matmult.c ****       {
 354              		.loc 1 52 0 is_stmt 0 discriminator 1
 355 0409 8B45D0   		movl	-48(%rbp), %eax
 356 040c 3B45DC   		cmpl	-36(%rbp), %eax
 357 040f 0F8C84FE 		jl	.L15
 357      FFFF
  58:src/matrix/matmult.c ****       }
  59:src/matrix/matmult.c **** 
  60:src/matrix/matmult.c ****       for (; j < to; ++j)
 358              		.loc 1 60 0 is_stmt 1
 359 0415 EB59     		jmp	.L16
 360              	.L17:
  61:src/matrix/matmult.c ****       {
  62:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
 361              		.loc 1 62 0 discriminator 2
 362 0417 8B45D0   		movl	-48(%rbp), %eax
 363 041a 4898     		cltq
 364 041c 488D1485 		leaq	0(,%rax,4), %rdx
 364      00000000 
 365 0424 488B45F0 		movq	-16(%rbp), %rax
 366 0428 4801D0   		addq	%rdx, %rax
 367 042b 8B00     		movl	(%rax), %eax
 368 042d 4898     		cltq
 369 042f 488D14C5 		leaq	0(,%rax,8), %rdx
 369      00000000 
 370 0437 488B45B0 		movq	-80(%rbp), %rax
 371 043b 4801D0   		addq	%rdx, %rax
 372 043e F20F1008 		movsd	(%rax), %xmm1
 373 0442 8B45D0   		movl	-48(%rbp), %eax
 374 0445 4898     		cltq
 375 0447 488D14C5 		leaq	0(,%rax,8), %rdx
 375      00000000 
 376 044f 488B45F8 		movq	-8(%rbp), %rax
 377 0453 4801D0   		addq	%rdx, %rax
 378 0456 F20F1000 		movsd	(%rax), %xmm0
 379 045a F20F59C1 		mulsd	%xmm1, %xmm0
 380 045e F20F104D 		movsd	-32(%rbp), %xmm1
 380      E0
 381 0463 F20F58C1 		addsd	%xmm1, %xmm0
 382 0467 F20F1145 		movsd	%xmm0, -32(%rbp)
 382      E0
  60:src/matrix/matmult.c ****       {
 383              		.loc 1 60 0 discriminator 2
 384 046c 8345D001 		addl	$1, -48(%rbp)
 385              	.L16:
  60:src/matrix/matmult.c ****       {
 386              		.loc 1 60 0 is_stmt 0 discriminator 1
 387 0470 8B45D0   		movl	-48(%rbp), %eax
 388 0473 3B45DC   		cmpl	-36(%rbp), %eax
 389 0476 7C9F     		jl	.L17
 390 0478 EB69     		jmp	.L18
 391              	.L13:
  63:src/matrix/matmult.c ****       }
  64:src/matrix/matmult.c ****     }
  65:src/matrix/matmult.c ****     else
  66:src/matrix/matmult.c ****     {
  67:src/matrix/matmult.c ****       for (j = from; j < to; ++j)
 392              		.loc 1 67 0 is_stmt 1
 393 047a 8B45D8   		movl	-40(%rbp), %eax
 394 047d 8945D0   		movl	%eax, -48(%rbp)
 395 0480 EB59     		jmp	.L19
 396              	.L20:
  68:src/matrix/matmult.c ****       {
  69:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
 397              		.loc 1 69 0 discriminator 3
 398 0482 8B45D0   		movl	-48(%rbp), %eax
 399 0485 4898     		cltq
 400 0487 488D1485 		leaq	0(,%rax,4), %rdx
 400      00000000 
 401 048f 488B45F0 		movq	-16(%rbp), %rax
 402 0493 4801D0   		addq	%rdx, %rax
 403 0496 8B00     		movl	(%rax), %eax
 404 0498 4898     		cltq
 405 049a 488D14C5 		leaq	0(,%rax,8), %rdx
 405      00000000 
 406 04a2 488B45B0 		movq	-80(%rbp), %rax
 407 04a6 4801D0   		addq	%rdx, %rax
 408 04a9 F20F1008 		movsd	(%rax), %xmm1
 409 04ad 8B45D0   		movl	-48(%rbp), %eax
 410 04b0 4898     		cltq
 411 04b2 488D14C5 		leaq	0(,%rax,8), %rdx
 411      00000000 
 412 04ba 488B45F8 		movq	-8(%rbp), %rax
 413 04be 4801D0   		addq	%rdx, %rax
 414 04c1 F20F1000 		movsd	(%rax), %xmm0
 415 04c5 F20F59C1 		mulsd	%xmm1, %xmm0
 416 04c9 F20F104D 		movsd	-32(%rbp), %xmm1
 416      E0
 417 04ce F20F58C1 		addsd	%xmm1, %xmm0
 418 04d2 F20F1145 		movsd	%xmm0, -32(%rbp)
 418      E0
  67:src/matrix/matmult.c ****       {
 419              		.loc 1 67 0 discriminator 3
 420 04d7 8345D001 		addl	$1, -48(%rbp)
 421              	.L19:
  67:src/matrix/matmult.c ****       {
 422              		.loc 1 67 0 is_stmt 0 discriminator 1
 423 04db 8B45D0   		movl	-48(%rbp), %eax
 424 04de 3B45DC   		cmpl	-36(%rbp), %eax
 425 04e1 7C9F     		jl	.L20
 426              	.L18:
  70:src/matrix/matmult.c ****       }
  71:src/matrix/matmult.c ****     }
  72:src/matrix/matmult.c ****     res[i] = temp;
 427              		.loc 1 72 0 is_stmt 1 discriminator 2
 428 04e3 8B45CC   		movl	-52(%rbp), %eax
 429 04e6 4898     		cltq
 430 04e8 488D14C5 		leaq	0(,%rax,8), %rdx
 430      00000000 
 431 04f0 488B45A8 		movq	-88(%rbp), %rax
 432 04f4 4801D0   		addq	%rdx, %rax
 433 04f7 F20F1045 		movsd	-32(%rbp), %xmm0
 433      E0
 434 04fc F20F1100 		movsd	%xmm0, (%rax)
 435              	.LBE2:
  45:src/matrix/matmult.c ****   {
 436              		.loc 1 45 0 discriminator 2
 437 0500 8345CC01 		addl	$1, -52(%rbp)
 438              	.L12:
  45:src/matrix/matmult.c ****   {
 439              		.loc 1 45 0 is_stmt 0 discriminator 1
 440 0504 8B45CC   		movl	-52(%rbp), %eax
 441 0507 3B45D4   		cmpl	-44(%rbp), %eax
 442 050a 0F8C30FD 		jl	.L21
 442      FFFF
  73:src/matrix/matmult.c ****   }
  74:src/matrix/matmult.c **** }...
 443              		.loc 1 74 0 is_stmt 1
 444 0510 90       		nop
 445 0511 C9       		leave
 446              		.cfi_def_cfa 7, 8
 447 0512 C3       		ret
 448              		.cfi_endproc
 449              	.LFE7:
 451              	.Letext0:
 452              		.file 2 "./src/include/genmat.h"
 453              		.file 3 "/usr/include/time.h"
