   1              		.file	"matmult.c"
   2              		.text
   3              	.Ltext0:
   4              		.p2align 4,,15
   5              		.globl	fullmatvectmult
   7              	fullmatvectmult:
   8              	.LFB18:
   9              		.file 1 "src/matrix/matmult.c"
   1:src/matrix/matmult.c **** #include "genmat.h"
   2:src/matrix/matmult.c **** #include <time.h>
   3:src/matrix/matmult.c **** #include <stdlib.h>
   4:src/matrix/matmult.c **** 
   5:src/matrix/matmult.c **** void fullmatvectmult(double *fullmat, double *vect, int size, double *res)
   6:src/matrix/matmult.c **** {
  10              		.loc 1 6 0
  11              		.cfi_startproc
  12              	.LVL0:
  13 0000 55       		pushq	%rbp
  14              		.cfi_def_cfa_offset 16
  15              		.cfi_offset 6, -16
  16 0001 4889E5   		movq	%rsp, %rbp
  17              		.cfi_def_cfa_register 6
  18 0004 FF150000 	1:	call	*mcount@GOTPCREL(%rip)
  18      0000
  19              		.loc 1 6 0
  20              	.LVL1:
   7:src/matrix/matmult.c ****   int i, j;
   8:src/matrix/matmult.c **** 
   9:src/matrix/matmult.c ****   for (i = 0; i < size; ++i)
  21              		.loc 1 9 0
  22 000a 85D2     		testl	%edx, %edx
  23 000c 7E49     		jle	.L1
  24 000e 8D42FF   		leal	-1(%rdx), %eax
  25 0011 4C63C2   		movslq	%edx, %r8
  26 0014 49C1E003 		salq	$3, %r8
  27 0018 4C8D4CC1 		leaq	8(%rcx,%rax,8), %r9
  27      08
  28 001d 488D14C5 		leaq	8(,%rax,8), %rdx
  28      08000000 
  29              	.LVL2:
  30              		.p2align 4,,10
  31 0025 0F1F00   		.p2align 3
  32              	.L3:
  33 0028 F20F1009 		movsd	(%rcx), %xmm1
   6:src/matrix/matmult.c ****   int i, j;
  34              		.loc 1 6 0
  35 002c 31C0     		xorl	%eax, %eax
  36              	.LVL3:
  37 002e 6690     		.p2align 4,,10
  38              		.p2align 3
  39              	.L4:
  10:src/matrix/matmult.c ****   {
  11:src/matrix/matmult.c ****     for (j = 0; j < size; ++j)
  12:src/matrix/matmult.c ****     {
  13:src/matrix/matmult.c ****       res[i] += fullmat[i * size + j] * vect[j];
  40              		.loc 1 13 0 discriminator 3
  41 0030 F20F1004 		movsd	(%rdi,%rax), %xmm0
  41      07
  42 0035 F20F5904 		mulsd	(%rsi,%rax), %xmm0
  42      06
  43 003a 4883C008 		addq	$8, %rax
  11:src/matrix/matmult.c ****     {
  44              		.loc 1 11 0 discriminator 3
  45 003e 4839C2   		cmpq	%rax, %rdx
  46              		.loc 1 13 0 discriminator 3
  47 0041 F20F58C8 		addsd	%xmm0, %xmm1
  48 0045 F20F1109 		movsd	%xmm1, (%rcx)
  11:src/matrix/matmult.c ****     {
  49              		.loc 1 11 0 discriminator 3
  50 0049 75E5     		jne	.L4
  51 004b 4883C108 		addq	$8, %rcx
  52 004f 4C01C7   		addq	%r8, %rdi
   9:src/matrix/matmult.c ****   {
  53              		.loc 1 9 0 discriminator 2
  54 0052 4939C9   		cmpq	%rcx, %r9
  55 0055 75D1     		jne	.L3
  56              	.L1:
  14:src/matrix/matmult.c ****     }
  15:src/matrix/matmult.c ****   }
  16:src/matrix/matmult.c **** }
  57              		.loc 1 16 0
  58 0057 5D       		popq	%rbp
  59              		.cfi_def_cfa 7, 8
  60 0058 C3       		ret
  61              		.cfi_endproc
  62              	.LFE18:
  64 0059 0F1F8000 		.p2align 4,,15
  64      000000
  65              		.globl	csmatvectmul
  67              	csmatvectmul:
  68              	.LFB19:
  17:src/matrix/matmult.c **** 
  18:src/matrix/matmult.c **** void csmatvectmul(CSrc *cs, double *vect, double *res)
  19:src/matrix/matmult.c **** {
  69              		.loc 1 19 0
  70              		.cfi_startproc
  71              	.LVL4:
  72 0060 55       		pushq	%rbp
  73              		.cfi_def_cfa_offset 16
  74              		.cfi_offset 6, -16
  75 0061 4889E5   		movq	%rsp, %rbp
  76              		.cfi_def_cfa_register 6
  77 0064 4154     		pushq	%r12
  78 0066 53       		pushq	%rbx
  79              		.cfi_offset 12, -24
  80              		.cfi_offset 3, -32
  81 0067 FF150000 	1:	call	*mcount@GOTPCREL(%rip)
  81      0000
  82              		.loc 1 19 0
  20:src/matrix/matmult.c ****   int i, j;
  21:src/matrix/matmult.c **** 
  22:src/matrix/matmult.c ****   int *ptr = cs->ptr;
  23:src/matrix/matmult.c ****   int *ind = cs->ind;
  24:src/matrix/matmult.c ****   double *val = cs->val;
  25:src/matrix/matmult.c ****   int n = cs->SData->n;
  83              		.loc 1 25 0
  84 006d 488B4718 		movq	24(%rdi), %rax
  22:src/matrix/matmult.c ****   int *ind = cs->ind;
  85              		.loc 1 22 0
  86 0071 4C8B5710 		movq	16(%rdi), %r10
  87              	.LVL5:
  23:src/matrix/matmult.c ****   double *val = cs->val;
  88              		.loc 1 23 0
  89 0075 488B5F08 		movq	8(%rdi), %rbx
  90              	.LVL6:
  24:src/matrix/matmult.c ****   int n = cs->SData->n;
  91              		.loc 1 24 0
  92 0079 4C8B27   		movq	(%rdi), %r12
  93              	.LVL7:
  94              		.loc 1 25 0
  95 007c 8B00     		movl	(%rax), %eax
  96              	.LVL8:
  26:src/matrix/matmult.c **** 
  27:src/matrix/matmult.c ****   for (i = 0; i < n; ++i)
  97              		.loc 1 27 0
  98 007e 85C0     		testl	%eax, %eax
  99 0080 7E63     		jle	.L7
 100 0082 83E801   		subl	$1, %eax
 101              	.LVL9:
 102 0085 4C8D5CC2 		leaq	8(%rdx,%rax,8), %r11
 102      08
 103              	.LVL10:
 104 008a 660F1F44 		.p2align 4,,10
 104      0000
 105              		.p2align 3
 106              	.L11:
  28:src/matrix/matmult.c ****   {
  29:src/matrix/matmult.c ****     for (j = ptr[i]; j < ptr[i + 1]; ++j)
 107              		.loc 1 29 0
 108 0090 496302   		movslq	(%r10), %rax
 109              	.LVL11:
 110 0093 418B4A04 		movl	4(%r10), %ecx
 111 0097 39C8     		cmpl	%ecx, %eax
 112 0099 7D3D     		jge	.L9
 113 009b 83E901   		subl	$1, %ecx
 114 009e F20F100A 		movsd	(%rdx), %xmm1
 115 00a2 29C1     		subl	%eax, %ecx
 116 00a4 4C8D0C83 		leaq	(%rbx,%rax,4), %r9
 117 00a8 4D8D04C4 		leaq	(%r12,%rax,8), %r8
 118 00ac 4883C101 		addq	$1, %rcx
 119 00b0 31C0     		xorl	%eax, %eax
 120              	.LVL12:
 121              		.p2align 4,,10
 122 00b2 660F1F44 		.p2align 3
 122      0000
 123              	.L10:
  30:src/matrix/matmult.c ****     {
  31:src/matrix/matmult.c ****       res[i] += vect[ind[j]] * val[j];
 124              		.loc 1 31 0 discriminator 3
 125 00b8 49633C81 		movslq	(%r9,%rax,4), %rdi
 126 00bc F20F1004 		movsd	(%rsi,%rdi,8), %xmm0
 126      FE
 127 00c1 F2410F59 		mulsd	(%r8,%rax,8), %xmm0
 127      04C0
 128 00c7 4883C001 		addq	$1, %rax
 129              	.LVL13:
  29:src/matrix/matmult.c ****     {
 130              		.loc 1 29 0 discriminator 3
 131 00cb 4839C1   		cmpq	%rax, %rcx
 132              		.loc 1 31 0 discriminator 3
 133 00ce F20F58C8 		addsd	%xmm0, %xmm1
 134 00d2 F20F110A 		movsd	%xmm1, (%rdx)
  29:src/matrix/matmult.c ****     {
 135              		.loc 1 29 0 discriminator 3
 136 00d6 75E0     		jne	.L10
 137              	.L9:
 138 00d8 4883C208 		addq	$8, %rdx
 139 00dc 4983C204 		addq	$4, %r10
  27:src/matrix/matmult.c ****   {
 140              		.loc 1 27 0
 141 00e0 4939D3   		cmpq	%rdx, %r11
 142 00e3 75AB     		jne	.L11
 143              	.L7:
  32:src/matrix/matmult.c ****     }
  33:src/matrix/matmult.c ****   }
  34:src/matrix/matmult.c **** }
 144              		.loc 1 34 0
 145 00e5 5B       		popq	%rbx
 146              	.LVL14:
 147 00e6 415C     		popq	%r12
 148              	.LVL15:
 149 00e8 5D       		popq	%rbp
 150              		.cfi_def_cfa 7, 8
 151 00e9 C3       		ret
 152              		.cfi_endproc
 153              	.LFE19:
 155 00ea 660F1F44 		.p2align 4,,15
 155      0000
 156              		.globl	csmatvectmul_opt
 158              	csmatvectmul_opt:
 159              	.LFB20:
  35:src/matrix/matmult.c **** 
  36:src/matrix/matmult.c **** void csmatvectmul_opt(CSrc *cs, double *vect, double *res)
  37:src/matrix/matmult.c **** {
 160              		.loc 1 37 0
 161              		.cfi_startproc
 162              	.LVL16:
 163 00f0 55       		pushq	%rbp
 164              		.cfi_def_cfa_offset 16
 165              		.cfi_offset 6, -16
 166 00f1 4889E5   		movq	%rsp, %rbp
 167              		.cfi_def_cfa_register 6
 168 00f4 4156     		pushq	%r14
 169 00f6 4155     		pushq	%r13
 170 00f8 4154     		pushq	%r12
 171 00fa 53       		pushq	%rbx
 172              		.cfi_offset 14, -24
 173              		.cfi_offset 13, -32
 174              		.cfi_offset 12, -40
 175              		.cfi_offset 3, -48
 176 00fb FF150000 	1:	call	*mcount@GOTPCREL(%rip)
 176      0000
 177              		.loc 1 37 0
  38:src/matrix/matmult.c ****   int i, j;
  39:src/matrix/matmult.c **** 
  40:src/matrix/matmult.c ****   int *ptr = cs->ptr;
  41:src/matrix/matmult.c ****   int *ind = cs->ind;
  42:src/matrix/matmult.c ****   double *val = cs->val;
  43:src/matrix/matmult.c ****   int n = cs->SData->n;
 178              		.loc 1 43 0
 179 0101 488B4718 		movq	24(%rdi), %rax
  40:src/matrix/matmult.c ****   int *ind = cs->ind;
 180              		.loc 1 40 0
 181 0105 4C8B4F10 		movq	16(%rdi), %r9
 182              	.LVL17:
  41:src/matrix/matmult.c ****   double *val = cs->val;
 183              		.loc 1 41 0
 184 0109 4C8B5708 		movq	8(%rdi), %r10
 185              	.LVL18:
  42:src/matrix/matmult.c ****   int n = cs->SData->n;
 186              		.loc 1 42 0
 187 010d 488B1F   		movq	(%rdi), %rbx
 188              	.LVL19:
 189              		.loc 1 43 0
 190 0110 8B00     		movl	(%rax), %eax
 191              	.LVL20:
  44:src/matrix/matmult.c **** 
  45:src/matrix/matmult.c ****   for (i = 0; i < n; ++i)
 192              		.loc 1 45 0
 193 0112 85C0     		testl	%eax, %eax
 194 0114 7E7E     		jle	.L14
 195 0116 83E801   		subl	$1, %eax
 196              	.LVL21:
 197 0119 4D8D6210 		leaq	16(%r10), %r12
 198 011d 4531C0   		xorl	%r8d, %r8d
 199 0120 4C8D1C85 		leaq	4(,%rax,4), %r11
 199      04000000 
 200              	.LVL22:
 201 0128 0F1F8400 		.p2align 4,,10
 201      00000000 
 202              		.p2align 3
 203              	.L21:
 204              	.LBB2:
  46:src/matrix/matmult.c ****   {
  47:src/matrix/matmult.c ****     double temp = 0;
  48:src/matrix/matmult.c ****     int from = ptr[i], to = ptr[i + 1];
 205              		.loc 1 48 0
 206 0130 4F632C01 		movslq	(%r9,%r8), %r13
 207              	.LVL23:
 208 0134 438B7C01 		movl	4(%r9,%r8), %edi
 208      04
 209              	.LVL24:
  49:src/matrix/matmult.c **** 
  50:src/matrix/matmult.c ****     if (to - from > 3)
 210              		.loc 1 50 0
 211 0139 89F8     		movl	%edi, %eax
 212 013b 4429E8   		subl	%r13d, %eax
 213 013e 83F803   		cmpl	$3, %eax
 214 0141 7F5D     		jg	.L16
 215              	.LVL25:
  51:src/matrix/matmult.c ****     {
  52:src/matrix/matmult.c ****       for (j = from; j < to; j += 4)
  53:src/matrix/matmult.c ****       {
  54:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
  55:src/matrix/matmult.c ****         temp += vect[ind[j+1]] * val[j+1];
  56:src/matrix/matmult.c ****         temp += vect[ind[j+2]] * val[j+2];
  57:src/matrix/matmult.c ****         temp += vect[ind[j+3]] * val[j+3];
  58:src/matrix/matmult.c ****       }
  59:src/matrix/matmult.c **** 
  60:src/matrix/matmult.c ****       for (; j < to; ++j)
  61:src/matrix/matmult.c ****       {
  62:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
  63:src/matrix/matmult.c ****       }
  64:src/matrix/matmult.c ****     }
  65:src/matrix/matmult.c ****     else
  66:src/matrix/matmult.c ****     {
  67:src/matrix/matmult.c ****       for (j = from; j < to; ++j)
 216              		.loc 1 67 0
 217 0143 4139FD   		cmpl	%edi, %r13d
 218 0146 0F8DF400 		jge	.L22
 218      0000
 219 014c 83EF01   		subl	$1, %edi
 220              	.LVL26:
  47:src/matrix/matmult.c ****     int from = ptr[i], to = ptr[i + 1];
 221              		.loc 1 47 0
 222 014f 660FEFC0 		pxor	%xmm0, %xmm0
 223 0153 4429EF   		subl	%r13d, %edi
 224 0156 4F8D34AA 		leaq	(%r10,%r13,4), %r14
 225 015a 4E8D2CEB 		leaq	(%rbx,%r13,8), %r13
 226 015e 4883C701 		addq	$1, %rdi
 227              		.loc 1 67 0
 228 0162 31C0     		xorl	%eax, %eax
 229              	.LVL27:
 230              		.p2align 4,,10
 231 0164 0F1F4000 		.p2align 3
 232              	.L20:
  68:src/matrix/matmult.c ****       {
  69:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
 233              		.loc 1 69 0 discriminator 3
 234 0168 49630C86 		movslq	(%r14,%rax,4), %rcx
 235 016c F20F100C 		movsd	(%rsi,%rcx,8), %xmm1
 235      CE
 236 0171 F2410F59 		mulsd	0(%r13,%rax,8), %xmm1
 236      4CC500
 237 0178 4883C001 		addq	$1, %rax
 238              	.LVL28:
  67:src/matrix/matmult.c ****       {
 239              		.loc 1 67 0 discriminator 3
 240 017c 4839C7   		cmpq	%rax, %rdi
 241              		.loc 1 69 0 discriminator 3
 242 017f F20F58C1 		addsd	%xmm1, %xmm0
 243              	.LVL29:
  67:src/matrix/matmult.c ****       {
 244              		.loc 1 67 0 discriminator 3
 245 0183 75E3     		jne	.L20
  70:src/matrix/matmult.c ****       }
  71:src/matrix/matmult.c ****     }
  72:src/matrix/matmult.c ****     res[i] = temp;
 246              		.loc 1 72 0 discriminator 2
 247 0185 F2420F11 		movsd	%xmm0, (%rdx,%r8,2)
 247      0442
 248 018b 4983C004 		addq	$4, %r8
 249              	.LVL30:
 250              	.LBE2:
  45:src/matrix/matmult.c ****   {
 251              		.loc 1 45 0 discriminator 2
 252 018f 4D39C3   		cmpq	%r8, %r11
 253 0192 759C     		jne	.L21
 254              	.LVL31:
 255              	.L14:
  73:src/matrix/matmult.c ****   }
  74:src/matrix/matmult.c **** }...
 256              		.loc 1 74 0
 257 0194 5B       		popq	%rbx
 258              	.LVL32:
 259 0195 415C     		popq	%r12
 260 0197 415D     		popq	%r13
 261 0199 415E     		popq	%r14
 262 019b 5D       		popq	%rbp
 263              		.cfi_remember_state
 264              		.cfi_def_cfa 7, 8
 265 019c C3       		ret
 266              	.LVL33:
 267 019d 0F1F00   		.p2align 4,,10
 268              		.p2align 3
 269              	.L16:
 270              		.cfi_restore_state
 271              	.LBB3:
  52:src/matrix/matmult.c ****       for (j = from; j < to; j += 4)
 272              		.loc 1 52 0
 273 01a0 4139FD   		cmpl	%edi, %r13d
 274 01a3 0F8D9700 		jge	.L22
 274      0000
 275 01a9 83EF01   		subl	$1, %edi
 276              	.LVL34:
 277 01ac 4D63F5   		movslq	%r13d, %r14
 278 01af 4C29EF   		subq	%r13, %rdi
  47:src/matrix/matmult.c ****     double temp = 0;
 279              		.loc 1 47 0
 280 01b2 660FEFC0 		pxor	%xmm0, %xmm0
 281 01b6 83E7FC   		andl	$4294967292, %edi
 282 01b9 4B8D04B2 		leaq	(%r10,%r14,4), %rax
 283 01bd 4A8D0CF3 		leaq	(%rbx,%r14,8), %rcx
 284 01c1 4C01F7   		addq	%r14, %rdi
 285 01c4 4D8D2CBC 		leaq	(%r12,%rdi,4), %r13
 286              	.LVL35:
 287 01c8 0F1F8400 		.p2align 4,,10
 287      00000000 
 288              		.p2align 3
 289              	.L19:
  54:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
 290              		.loc 1 54 0 discriminator 3
 291 01d0 486338   		movslq	(%rax), %rdi
 292 01d3 4883C010 		addq	$16, %rax
 293 01d7 4883C120 		addq	$32, %rcx
 294 01db F20F100C 		movsd	(%rsi,%rdi,8), %xmm1
 294      FE
  55:src/matrix/matmult.c ****         temp += vect[ind[j+1]] * val[j+1];
 295              		.loc 1 55 0 discriminator 3
 296 01e0 486378F4 		movslq	-12(%rax), %rdi
  54:src/matrix/matmult.c ****         temp += vect[ind[j]] * val[j];
 297              		.loc 1 54 0 discriminator 3
 298 01e4 F20F5949 		mulsd	-32(%rcx), %xmm1
 298      E0
 299 01e9 F20F58C1 		addsd	%xmm1, %xmm0
 300              	.LVL36:
  55:src/matrix/matmult.c ****         temp += vect[ind[j+1]] * val[j+1];
 301              		.loc 1 55 0 discriminator 3
 302 01ed F20F100C 		movsd	(%rsi,%rdi,8), %xmm1
 302      FE
  56:src/matrix/matmult.c ****         temp += vect[ind[j+2]] * val[j+2];
 303              		.loc 1 56 0 discriminator 3
 304 01f2 486378F8 		movslq	-8(%rax), %rdi
  55:src/matrix/matmult.c ****         temp += vect[ind[j+1]] * val[j+1];
 305              		.loc 1 55 0 discriminator 3
 306 01f6 F20F5949 		mulsd	-24(%rcx), %xmm1
 306      E8
 307 01fb F20F58C8 		addsd	%xmm0, %xmm1
 308              	.LVL37:
  56:src/matrix/matmult.c ****         temp += vect[ind[j+2]] * val[j+2];
 309              		.loc 1 56 0 discriminator 3
 310 01ff F20F1004 		movsd	(%rsi,%rdi,8), %xmm0
 310      FE
  57:src/matrix/matmult.c ****         temp += vect[ind[j+3]] * val[j+3];
 311              		.loc 1 57 0 discriminator 3
 312 0204 486378FC 		movslq	-4(%rax), %rdi
  56:src/matrix/matmult.c ****         temp += vect[ind[j+2]] * val[j+2];
 313              		.loc 1 56 0 discriminator 3
 314 0208 F20F5941 		mulsd	-16(%rcx), %xmm0
 314      F0
 315 020d F20F58C8 		addsd	%xmm0, %xmm1
 316              	.LVL38:
  57:src/matrix/matmult.c ****         temp += vect[ind[j+3]] * val[j+3];
 317              		.loc 1 57 0 discriminator 3
 318 0211 F20F1004 		movsd	(%rsi,%rdi,8), %xmm0
 318      FE
 319 0216 F20F5941 		mulsd	-8(%rcx), %xmm0
 319      F8
  52:src/matrix/matmult.c ****       for (j = from; j < to; j += 4)
 320              		.loc 1 52 0 discriminator 3
 321 021b 4939C5   		cmpq	%rax, %r13
  57:src/matrix/matmult.c ****         temp += vect[ind[j+3]] * val[j+3];
 322              		.loc 1 57 0 discriminator 3
 323 021e F20F58C1 		addsd	%xmm1, %xmm0
 324              	.LVL39:
  52:src/matrix/matmult.c ****       for (j = from; j < to; j += 4)
 325              		.loc 1 52 0 discriminator 3
 326 0222 75AC     		jne	.L19
  72:src/matrix/matmult.c ****     res[i] = temp;
 327              		.loc 1 72 0
 328 0224 F2420F11 		movsd	%xmm0, (%rdx,%r8,2)
 328      0442
 329 022a 4983C004 		addq	$4, %r8
 330              	.LVL40:
 331              	.LBE3:
  45:src/matrix/matmult.c ****   for (i = 0; i < n; ++i)
 332              		.loc 1 45 0
 333 022e 4D39C3   		cmpq	%r8, %r11
 334 0231 0F85F9FE 		jne	.L21
 334      FFFF
 335 0237 E958FFFF 		jmp	.L14
 335      FF
 336              	.LVL41:
 337 023c 0F1F4000 		.p2align 4,,10
 338              		.p2align 3
 339              	.L22:
 340              	.LBB4:
  47:src/matrix/matmult.c ****     double temp = 0;
 341              		.loc 1 47 0
 342 0240 660FEFC0 		pxor	%xmm0, %xmm0
 343              	.LVL42:
  72:src/matrix/matmult.c ****     res[i] = temp;
 344              		.loc 1 72 0
 345 0244 F2420F11 		movsd	%xmm0, (%rdx,%r8,2)
 345      0442
 346 024a 4983C004 		addq	$4, %r8
 347              	.LVL43:
 348              	.LBE4:
  45:src/matrix/matmult.c ****   for (i = 0; i < n; ++i)
 349              		.loc 1 45 0
 350 024e 4D39C3   		cmpq	%r8, %r11
 351 0251 0F85D9FE 		jne	.L21
 351      FFFF
 352 0257 E938FFFF 		jmp	.L14
 352      FF
 353              		.cfi_endproc
 354              	.LFE20:
 356              	.Letext0:
 357              		.file 2 "./src/include/genmat.h"
 358              		.file 3 "/usr/include/time.h"
