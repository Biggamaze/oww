#include <stdio.h>
#include <sys/times.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include"pomiar_czasu.h"

#include<sys/time.h>
#include<sys/resource.h>
static struct rusage rp;

#define K (1024)
#define M (K*K)
//#define CACHE_MIN (K)
#define CACHE_MIN (M)
#define CACHE_MAX (M)
//#define CACHE_MAX (128*M)
/* time sample and clock tick */
#define SAMPLE (1)
#ifndef CLK_TCK
#define CLK_TCK (100)
#endif
 	

#define min(X,Y) ((X) < (Y) ?  (X) : (Y))

double get_seconds() { /* to read time */
  struct tms rusage;
  
  times(&rusage);
  return ((double)(rusage.tms_utime) / CLK_TCK);
}

double get_CPU_seconds() { /* to read time */
  
  struct rusage rk;
  double cputime;
  
  getrusage(RUSAGE_SELF, &rk);
  
  cputime = (rk.ru_utime.tv_usec-rp.ru_utime.tv_usec)/1e6;
  cputime += rk.ru_utime.tv_sec-rp.ru_utime.tv_sec;
  
  return (cputime);
}

long get_page_faults() { /* to read time */
  
  struct rusage rk;
  long page_faults;
  
  getrusage(RUSAGE_SELF, &rk);
  
  page_faults = rk.ru_majflt;
  
  return (page_faults);
}


double lg(double x) { /* log base 2 */
   return (log(x)/log(2.0));
}

double cache[CACHE_MAX];  /* stride through various parts of this array */


int main() {
    int register csize;    /* outer loop "cache" size */
    int register stride;   /* inner loop stride through the cache */
    int register lim;      /* upper limit cache reference for each stride */
    int register i;        /* loop indices */
    int register j;
    int register dummy;    /* for dummy loop equivalent of cache access */
    int register steps;    /* number of while loop iterations */
    int register dsteps;   /* ditto for dummy loop */
    double sec0,sec_0, secwc0;           /* start time */
    double sec, sec_, secwc;            /* time accumulator */
    long nr_page_faults;
    /* want a comma separated variable file, so first dump the headers 
    printf
      ("\"lg(size [B]/[B])\",\"lg(stride [B]/[B])\",\"r+w time [ns]\"\n");*/
    /* vary "cache" from min to max in powers of 2 */
    for (csize = CACHE_MIN; csize <= CACHE_MAX; csize *= 2) {
        /* vary stride from 1 thru csize/2 in powers of 2 */
        for (stride = 1; stride < csize ; stride *= 2) {
            sec = 0; /* initialize time accumulator */
            sec_ = 0; /* initialize time accumulator */
            secwc = 0; /* initialize time accumulator */
            lim = csize - stride + 1; /* upper cache ref limit this stride */


	    memset (cache, 0, sizeof(cache)); /* initialize the array */

	    nr_page_faults = get_page_faults();
	    
	    steps = 0; /* total number of while iterations */
	    while (sec < 0.4) { /* repeat till we've run for 1 second */
	      sec0 = get_CPU_seconds(); /* start timer */
	      sec_0 = get_seconds(); /* start timer */
	      secwc0 = czas_zegara();
	      /* outer for loop allows eviction from cache (if needed) */
	      /* amortizes the eviction cost (if any) and */
	      /* gives a better picture of average memory access time */
	      for (i = SAMPLE * stride; i != 0; i--) {
		/* inner loop does the actual read and write of memory */
		for (j = 0; j < lim; j += stride) {
		  //	  get_seconds();get_seconds();get_seconds();get_seconds();get_seconds();
		  cache[j]++; /* r+w one location in memory */
		} /* for j */
	      } /* for i */
	      steps++; /* number of while loop iterations */
	      sec += (get_CPU_seconds() - sec0); /* add to time so far */
	      sec_ += (get_seconds() - sec_0); /* add to time so far */
	      secwc += (czas_zegara()-secwc0);
	    } /* while sec */
	    
            nr_page_faults =  get_page_faults() - nr_page_faults;
	    
            /* subtract loop overhead, run dummy (non-memory) ver of loop */
            dummy = 0; /* initialize dummy cache (a register variable) */
            dsteps = 0; /* want same number of steps as above */
            while (dsteps < steps) { /* repeat for same number of steps */
	      sec0 = get_CPU_seconds(); /* start timer */
	      sec_0 = get_seconds(); /* start timer */
	      secwc0 = czas_zegara();
	      for (i = SAMPLE * stride; i != 0; i--) {
		for (j = 0; j < lim; j += stride) {
		  dummy++; /* r+w non -memory loc(register variable) */
		}
	      }
	      dsteps++; /* record iterations */
	      //printf("stare %lf(%lf), nowe %lf(%lf)\n", sec,sec_,(get_CPU_seconds() - sec0), (get_seconds() - sec_0));
	      sec -= (get_CPU_seconds() - sec0); /* subtract loop OH time */
	      sec_ -= (get_seconds() - sec_0); /* subtract loop OH time */
	      secwc -= (czas_zegara()-secwc0);
            } /* while dsteps */
	    
            /* for each size and stride, print time per access [ns] */
            /* NOTE: # accesses = steps*SAMPLE*stride*((lim-1)/stride+1)) */
	    //            printf("\"%6.0f\",\"%6.0f\",\"%14.2f\"(%14.2f)\n",
	    if(stride==1) printf("\n", dummy);
            printf("%6.0f %6.0f %14.2f %14.2f %14.2f %10ld\n",
		   lg(csize * sizeof(int)),
		   lg(stride * sizeof(int)),
		   sec_ * 1e9 / (steps*SAMPLE*stride * ((lim - 1) / stride + 1)),
		   secwc * 1e9 / (steps*SAMPLE*stride * ((lim - 1) / stride + 1)),
		   min(sec * 1e9 / (steps*SAMPLE*stride * ((lim - 1) / stride + 1)), 
		       min( sec_ * 1e9 / (steps*SAMPLE*stride * ((lim - 1) / stride + 1)),
			    secwc * 1e9 / (steps*SAMPLE*stride * ((lim - 1) / stride + 1)))),
		   nr_page_faults);
        } /* for stride */
    } /* for csize */
} /* main */
