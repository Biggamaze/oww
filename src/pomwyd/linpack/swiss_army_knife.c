#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <emmintrin.h>
#include "pomiar_czasu.h"

double funkcja(double * a, double* b, double* c, double* d, int n){
  
  int i;
  for(i=0;i<n;i++) {
    a[i] = b[i]*c[i]-d[i];
    if(a[i]>b[i]) c[i]+=d[i];
  }
  return(a[0]+a[n]);
  
}


int main(int argc, char* argv[])
{
  int N, NN, ntimes, i, j, it;
  double t_s, rmstime;
  double *a = NULL;
  double *b = NULL;
  double *c = NULL;
  double *d = NULL;
  register double dot, dot1, sum_dot;
  const int L1 = 4096;  //ilosc slow double w cache L1 = 32 K
  
  N = 2000000;
  
  N = N/L1;
  N = N*L1; 
  
  
  a = (double *)_mm_malloc(N*sizeof(double), 16);
  b = (double *)_mm_malloc(N*sizeof(double), 16);
  c = (double *)_mm_malloc(N*sizeof(double), 16);
  d = (double *)_mm_malloc(N*sizeof(double), 16);


  int kk; double tt; double tt1=0.0;

  for(NN=100;NN<N;NN*=1.11){ 
    
   
    
    for(i=0; i<NN; i++)
      {
	a[i] = sqrt((double)(i+1));
	b[i] = sqrt((double)(i+1));
	c[i] = sqrt((double)(i+1));
	d[i] = sqrt((double)(i+1));
      }
    
    int NTIMES = 10*N/NN;
    //printf("N = %d, ntimes = %d\n",NN,NTIMES); 

    tt = czas_zegara();
    
    for (kk=0; kk<NTIMES; kk++)
      {
	
        
        for (j=0; j<NN; j++)
	  a[j] = b[j]+d[j]*c[j];

	j=NN/2;	
	rmstime = d[j]+c[j]*a[j];
        if (rmstime < -1.0e-4) rmstime += funkcja(a,b,c,d,NN);
	//printf("kk %d\n",kk);
      }
    
    tt1 = czas_zegara() - tt;
    
    //printf("rms %lf\n",rmstime);
    
    //printf("Rate (MB/s)              time\n");
    
    printf("%20d %11.4f  %16.14f   \n",
	   NN, 1.0E-06 * 4 * sizeof(double) * NN/(tt1/NTIMES+1.e-12),  
	   tt1/NTIMES);
    
    
    rmstime += funkcja(a,b,c,d,NN);
    
  }
  
  printf("%lf\n",rmstime);
  return 0;
}

