#include <stdlib.h>
#include <math.h>
#include "genrnd.h"
#include <stdlib.h>
#include <stdio.h>

int isavail(int *vect, int size, int rn);
int icompare (const void * a, const void * b);
void randivectasc_sparse(int count, int min, int max, int *vect);
void randivectasc_full(int count ,int min, int max, int *vect);

double randbase()
{
  return (double)rand() / RAND_MAX;
}

int randint(int min, int max)
{
  return (int)randdbl((double)min, (double)max);
}

double randdbl(double min, double max)
{
  double base = randbase();
  return (base * (max - min) + min);
}

void randdvect(int count, double min, double max, double* vect) {
  int i;
  for (i = 0; i < count; ++i) {
    vect[i] = randdbl(min, max);
  }
}

void randivectasc(int count ,int min, int max, int *vect)
{
  if ( (double)count / ((double)max - (double)min) > 0.8) 
  {
    randivectasc_full(count, min, max, vect);
  } else 
  {
    randivectasc_sparse(count, min, max, vect);
  }
}

// <min, max)
void randivectasc_full(int count ,int min, int max, int *vect)
{
  int range = max - min;
  int in, im;

  im = 0;

  for (in = 0; in < range && im < count; ++in)
  {
    int rn = range - in;
    int rm = count - im;
    if (rand() % rn < rm)
      vect[im++] = in + min; /* +min since your range begins from min */
  }
}

void randivectasc_sparse(int count, int min, int max, int *vect)
{
  int rn, i;
  for (i = 0; i < count; ++i)
  {
    do
    {
      rn = randint(min, max);
    } while (isavail(vect, i, rn) != 1);

    vect[i] = rn;
  }

  qsort(vect, count, sizeof(int), icompare);
}

int isavail(int *vect, int size, int rn)
{
  int i, res = 1;
  for (i = 0; i < size; ++i)
  {
    if (vect[i] == rn)
    {
      res = 0;
      break;
    }
  }
  return res;
}

int icompare (const void * a, const void * b)
{
    int _a = *(int*)a;
    int _b = *(int*)b;
    if(_a < _b) return -1;
    else if(_a == _b) return 0;
    else return 1;
}