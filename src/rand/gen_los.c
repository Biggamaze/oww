#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

const int liczba = 20;

void main1()
{

#define DEBUG

  double a, b, c;
  int i, j, k, l;
  FILE *f;
  f = fopen("./gnuplot/results.txt", "w");

  srand(time(NULL));

  printf("Podaj dwie liczby rzeczywiste\n");
  scanf("%lf %lf", &a, &b);
  printf("Losowe liczby rzeczywiste z zakresu : %lf - %lf\n", a, b);

  for (i = 0; i < liczba; i++)
  {
    double interval = b - a;
    c = fmod(rand(), interval) + a;
    printf("%lf ", c);
    fprintf(f, "%d %lf\n", i+1, c);

#ifdef DEBUG
    if (c < a || c > b)
    {
      printf("Blad generacji %lf\n", c);
      exit(0);
    }
#endif
  }

  printf("\n\n");
  fprintf(f, "\n\n");


  printf("Podaj dwie liczby calkowite\n");
  scanf("%d %d", &j, &k);
  printf("Losowe liczby calkowite z zakresu : %d - %d\n", j, k);

  for (i = 0; i < liczba; i++)
  {

    int intervalZ = k - j;
    l = (rand() % intervalZ) + j;

    printf("%d ",l);
    fprintf(f, "%d %d \n", i+1, l);

#ifdef DEBUG
    if (l < j || l > k)
    {
      printf("Blad generacji %d\n", l);
      exit(0);
    }
#endif
  }
  fclose(f);
}

