#ifndef MRND
#define MRND

int randint(int min, int max);
double randdbl(double min, double max);
void randivectasc(int count ,int min, int max, int *vect);
void randdvect(int count, double min, double max, double *vect);

#endif