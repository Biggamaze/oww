#include "genmat.h"

#ifndef MMATMULT
#define MMATMULT

void fullmatvectmult(double *fullmat, double *vect, int size, double *res);
void csmatvectmul(CSrc *cs, double *vect, double *res);
void csmatvectmul_opt(CSrc *cs, double *vect, double *res);
void csmatvectmul_par(CSrc *cs, double *vect, double *res);

#endif