#ifndef MMAT
#define MMAT

typedef struct {
  int n;
  int nnzw;
} StorageData;

typedef struct {
  double *val;
  int *ind;
  int *ptr;
  StorageData *SData;
} CSrc;

#define MAXDIAG 2
#define MINDIAG 1
#define MAXNDIAG 1
#define MINNDIAG 0

double *tofullmat(CSrc *cs);
void printfull(double *arr, int n);
void printcrsfull(CSrc *cs);

CSrc *sparsemat_crs(int n, int nnzw);
CSrc *sparseband_crs(int n, int d, int nnzw);

#endif