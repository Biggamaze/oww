#include "genmat.h"
#include <stdlib.h>
#include <stdio.h>

double *tofullmat(CSrc *cs) {
  int i,j,k,zeroIndex;
  int n = cs -> SData -> n;

  double *fmat = calloc(n*n, sizeof(double));

  for (i = 0; i < n; ++i) 
  {
    zeroIndex = 0;
    for (k = (cs -> ptr[i]); k < cs -> ptr[i+1]; k++)
    {
      j = cs -> ind[k];

      while (zeroIndex < j) {
        zeroIndex++;
      }

      fmat[i*n+zeroIndex] = cs -> val[k];
      // printf(" %lf ", cs -> val[k]);

      zeroIndex++;
    }

    while(zeroIndex < n) {
      zeroIndex++;
    }
  }

  return fmat;
}

void printfull(double *arr, int n) {
  int i,j;

  printf("\n");
  for (i = 0; i < n; ++i) {
    for (j = 0; j < n; ++j) {
      printf(" %lf ", arr[i*n + j]);
    }
    printf("\n");
  }
}