#include "genmat.h"
#include <stdio.h>

void printcrsfull(CSrc *crs) {
  int i,j,k,zeroIndex;
  int n = crs -> SData -> n;

  for (i = 0; i < n; ++i) 
  {
    zeroIndex = 0;
    for (k = (crs -> ptr[i]); k < crs -> ptr[i+1]; k++)
    {
      j = crs -> ind[k];

      while (zeroIndex < j) {
        printf(" %lf ", 0.0);
        zeroIndex++;
      }

      printf(" %lf ", crs -> val[k]);

      zeroIndex++;
    }

    while(zeroIndex < n) {
      printf(" %lf ", 0.0);
      zeroIndex++;
    }
    printf("\n");
  }
  printf("\n");
}