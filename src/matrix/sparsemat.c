#include "genrnd.h"
#include "genmat.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

CSrc *alloccs(int n, int nnzw);
int finddiag(int *vect, int l, int r, int d);
void makerow(CSrc *cs, int minpos, int maxpos, int nnzw, int i);

// macierze w formacie skompresowanym

CSrc *sparsemat_crs(int n, int nnzw)
{
  int i;
  CSrc *crs = alloccs(n, nnzw);

  for (i = 0; i < n; ++i)
  {
    makerow(crs, 0, n, nnzw, i);
  }

  crs->ptr[i] = n * nnzw;
  return crs;
}

CSrc *sparseband_crs(int n, int d, int nnzw)
{
  int i;
  CSrc *crs = alloccs(n, nnzw);

  for (i = 0; i < d; ++i)
  {
    makerow(crs, 0, i+d+1, nnzw, i);
  }
  for (; (n - i) > d; ++i)
  {
    makerow(crs, i-d, i+d, nnzw, i);
  }
  for (; i < n; ++i)
  {
    makerow(crs, i-d, n, nnzw, i);
  }

  crs->ptr[i] = n * nnzw;
  return crs;
}

// metody pomocnicze

void makerow(CSrc *cs, int minpos, int maxpos, int nnzw, int i)
{
  int diagp;
  randivectasc(nnzw, minpos, maxpos, &(cs->ind[i * nnzw]));
  randdvect(nnzw, MINNDIAG, MAXNDIAG, &(cs->val[i * nnzw]));
  diagp = finddiag(&(cs->ind[i * nnzw]), 0, nnzw - 1, i);

  if (diagp <= 0)
  {
    diagp *= (-1);
    cs->ind[i * nnzw + diagp] = i;
  }

  cs->val[i * nnzw + diagp] = randdbl(MINDIAG, MAXDIAG);
  cs->ptr[i] = i * nnzw;
}

CSrc *alloccs(int n, int nnzw)
{
  CSrc *crs = (CSrc *)malloc(sizeof(CSrc));
  crs->val = calloc(n * nnzw, sizeof(double));
  crs->ind = calloc(n * nnzw, sizeof(int));
  crs->ptr = calloc(n + 1, sizeof(int));
  crs->SData = malloc(sizeof(StorageData));

  (crs->SData)->nnzw = nnzw;
  (crs->SData)->n = n;

  return crs;
}

int finddiag(int *vect, int l, int r, int d)
{
  if (l < r)
  {
    int mid = l + (r - l) / 2;
    if (vect[mid] == d)
    {
      return mid;
    }
    else if (d < vect[mid])
    {
      return finddiag(vect, l, mid - 1, d);
    }
    else
    {
      return finddiag(vect, mid + 1, r, d);
    }
  }
  else
  {
    if (vect[l] == d)
    {
      return l;
    }
    return (-1) * l;
  }
}
