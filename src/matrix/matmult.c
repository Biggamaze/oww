#include "genmat.h"
#include <time.h>
#include <stdlib.h>
#include <omp.h>

void fullmatvectmult(double *fullmat, double *vect, int size, double *res)
{
  int i, j;

  for (i = 0; i < size; ++i)
  {
    for (j = 0; j < size; ++j)
    {
      res[i] += fullmat[i * size + j] * vect[j];
    }
  }
}

void csmatvectmul(CSrc *cs, double *vect, double *res)
{
  int i, j;

  int *ptr = cs->ptr;
  int *ind = cs->ind;
  double *val = cs->val;
  int n = cs->SData->n;

  for (i = 0; i < n; ++i)
  {
    for (j = ptr[i]; j < ptr[i + 1]; ++j)
    {
      res[i] += vect[ind[j]] * val[j];
    }
  }
}

void csmatvectmul_opt(CSrc *cs, double *vect, double *res)
{
  int i, j;

  int *ptr = cs->ptr;
  int *ind = cs->ind;
  double *val = cs->val;
  int n = cs->SData->n;

  for (i = 0; i < n; ++i)
  {
    double temp = 0;
    int from = ptr[i], to = ptr[i + 1];

    if (to - from > 3)
    {
      for (j = from; j < to; j += 4)
      {
        temp += vect[ind[j]] * val[j];
        temp += vect[ind[j+1]] * val[j+1];
        temp += vect[ind[j+2]] * val[j+2];
        temp += vect[ind[j+3]] * val[j+3];
      }

      for (; j < to; ++j)
      {
        temp += vect[ind[j]] * val[j];
      }
    }
    else
    {
      for (j = from; j < to; ++j)
      {
        temp += vect[ind[j]] * val[j];
      }
    }
    res[i] = temp;
  }
}

void csmatvectmul_par(CSrc *cs, double *vect, double *res)
{
  int i, j;

  int *ptr = cs->ptr;
  int *ind = cs->ind;
  double *val = cs->val;
  int n = cs->SData->n;

  #pragma omp parallel for default(none) shared(res, vect, ind, val, n, ptr) private(i, j) schedule(static, 1)
  for (i = 0; i < n; ++i)
  {
    int from = ptr[i], to = ptr[i + 1];  
    for (j = from; j < to; ++j)
    {
      res[i] += vect[ind[j]] * val[j];
    }
  }
}