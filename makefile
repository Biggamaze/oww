CCOMP=gcc

headers=./src/include
lib=./lib/
optl=O3
opt=-$(optl) -p -fPIC -Wall -fopenmp
rand=src/rand
matrix=src/matrix
test=test
pomiar=src/pomiarczasu

# definicje glowne
.PHONY : all
all : genrand genmat matmult pomiarczasu genrand_test genmat_sparse_test genmat_full_test matmult_test matmult_benchmark randvecti_test matmult_lab4

genrand: $(headers)/genrnd.h $(rand)/genrnd.c
	$(CCOMP) -c $(rand)/genrnd.c -o obj/genrnd.o -I$(headers)
	ar -cvq lib/libmrnd.a obj/genrnd.o
	ranlib lib/libmrnd.a

genmat: $(matrix)/sparsemat.c $(matrix)/fullmat.c $(matrix)/sparseprint.c genrand
	$(CCOMP) $(opt) -c $(matrix)/sparsemat.c -o obj/sparsemat.o -I$(headers) -L$(lib) -lmrnd 
	$(CCOMP) $(opt) -c $(matrix)/fullmat.c -o obj/fullmat.o -I$(headers)
	$(CCOMP) $(opt) -c $(matrix)/sparseprint.c -o obj/sparseprint.o -I$(headers)
	ar -cvq lib/libgenmat.a obj/sparsemat.o obj/sparseprint.o obj/fullmat.o obj/genrnd.o
	ranlib lib/libgenmat.a

matmult: $(matrix)/matmult.c genmat genrand
	$(CCOMP) $(opt) -c $(matrix)/matmult.c -o obj/matmult.o -I$(headers) -L$(lib) -lgenmat
	ar -cvq lib/libmatmult.a obj/matmult.o
	ranlib lib/libmatmult.a

pomiarczasu: $(pomiar)/pomiar_czasu.c
	$(CCOMP) $(opt) -c $(pomiar)/pomiar_czasu.c -o obj/pomiar.o -I$(headers)
	ar -cvq lib/libpomiar.a obj/pomiar.o	
	ranlib lib/libpomiar.a

# definicje testowe

genrand_test: $(test)/$(rand)/testrand.c genrand
	$(CCOMP) -o bin/rand.test obj/genrnd.o $(test)/$(rand)/testrand.c -I$(headers)

genmat_sparse_test: $(test)/$(matrix)/crs_sparse.c genmat
	$(CCOMP) $(opt) -o bin/genmat_sparse.test $(test)/$(matrix)/crs_sparse.c -I$(headers) -L$(lib) -lgenmat -lmrnd 
		
genmat_full_test: $(test)/$(matrix)/fulltest.c genmat genrand
	$(CCOMP) $(opt) -o bin/genmat_full.test $(test)/$(matrix)/fulltest.c -I$(headers) -L$(lib) -lgenmat -lmrnd

matmult_test: $(test)/$(matrix)/matmult_test.c matmult pomiarczasu
	$(CCOMP) $(opt) -o bin/matmult.test $(test)/$(matrix)/matmult_test.c -I$(headers) -L$(lib) -lmatmult -lgenmat -lpomiar

matmult_benchmark : $(test)/$(matrix)/matmult_benchmark.c matmult pomiarczasu
	$(CCOMP) $(opt) -o bin/matmult_benchmark.test $(test)/$(matrix)/matmult_benchmark.c -I$(headers) -L$(lib) -lmatmult -lgenmat -lpomiar

matmult_benchmark_lab8 : $(test)/$(matrix)/matmult_benchmark.lab8.c matmult pomiarczasu
	$(CCOMP) $(opt) -o bin/matmult_benchmark.lab8.test $(test)/$(matrix)/matmult_benchmark.lab8.c -I$(headers) -L$(lib) -lmatmult -lgenmat -lpomiar

matmult_lab4 : $(test)/$(matrix)/matmult_lab4.c matmult pomiarczasu
	$(CCOMP) $(opt) -o bin/matmult_lab4.test $(test)/$(matrix)/matmult_lab4.c -I$(headers) -L$(lib) -lmatmult -lgenmat -lpomiar

randvecti_test: $(test)/$(rand)/randvectint_test.c genrand pomiarczasu
	$(CCOMP) $(opt) -o bin/randvectint.test $(test)/$(rand)/randvectint_test.c -I$(headers) -L$(lib) -lmrnd -lpomiar

assembly:
	$(CCOMP) $(opt) -Wa,-adhln -c -g src/matrix/matmult.c -I$(headers) -L$(lib) -lmatmult -lgenmat -lpomiar > misc/assembly_lab8/$(optl).s
	rm matmult.o

.PHONY: clean
clean:
	mv bin/.gitignore .
	rm -f bin/* --preserve-root
	mv ./.gitignore bin/
	rm -f lib/*.a
	rm -f obj/*.o
	rm -f gmon.out
